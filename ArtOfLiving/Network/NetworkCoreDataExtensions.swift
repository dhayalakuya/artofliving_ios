//
//  NetworkCoreDataExtensions.swift
//  ArtOfLiving
//
//  Created by Dhayanithi on 14/02/18.
//  Copyright © 2018 Kuya. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreData

extension Program {
    
    func updateWithDictionary(json : [String:JSON]) {
        
        if let serverId = json[ProgramServerKey.id.rawValue]?.int {
            self.serverId = Int64(serverId)
        }

        if let name = json[ProgramServerKey.name.rawValue]?.stringValue {
            self.name = name
        }
        
        if let description = json[ProgramServerKey.description.rawValue]?.stringValue {
            self.programDescription = description
        }
        
        if let requirement = json[ProgramServerKey.requirement.rawValue]?.stringValue {
            self.requirement = requirement
        }
        
        if let isActive = json[ProgramServerKey.is_active.rawValue]?.intValue {
            self.isActive = Bool(truncating: NSNumber.init(value: isActive))
        }
        
        if let lastUpdated = json[ProgramServerKey.lastupdated.rawValue]?.stringValue {
            self.lastUpdated = lastUpdated
        }
    }
}

extension SubProgram {
    
    func updateWithDictionary(json : [String:JSON]) {
        
        if let serverId = json[SubProgramServerKey.id.rawValue]?.int {
            self.serverId = Int64(serverId)
        }
        
        if let name = json[SubProgramServerKey.name.rawValue]?.stringValue {
            self.name = name
        }
        
        if let typeId = json[SubProgramServerKey.type_id.rawValue]?.intValue {
            self.typeId = Int64(typeId)
        }

        if let requirement = json[SubProgramServerKey.requirements.rawValue]?.stringValue {
            self.requirements = requirement
        }
        
        if let isActive = json[SubProgramServerKey.is_active.rawValue]?.intValue {
            self.isActive = Bool(truncating: NSNumber.init(value: isActive))
        }
        
        if let lastUpdated = json[SubProgramServerKey.lastupdated.rawValue]?.stringValue {
            self.lastupdated = lastUpdated
        }
    }
}
