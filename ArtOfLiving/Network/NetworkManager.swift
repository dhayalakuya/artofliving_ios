//
//  NetworkManager.swift
//  ArtOfLiving
//
//  Created by Dhayanithi on 13/02/18.
//  Copyright © 2018 Kuya. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

public enum AOLEndPoints : String {
    case Types = "/gettypes"
    case SubTypes = "/getsubtypes"
    case Programs = "/programs"
}

@available(iOS 10.0, *)
@available(iOS 10.0, *)
class NetworkManager: NSObject {

    let baseURL = "https://programs.org.in/ekam/index.php/api/programs"
    
    public typealias SuccessClosure = (_ savedObject: AnyObject, _ json: JSON) -> Void
    public typealias FailureClosure = (_ error: NSError?) -> Void

    let sessionManager = SessionManager()
    let persistentService = PersistentService()

    func requestHeader(user: User?) -> [String: String] {
        
        let headers = [String:String]()
        return headers
    }
    
    func getProgramTypes(user: User?, failure: FailureClosure? = nil, success: ((_ savedObject: Any, _ json: JSON) -> Void)? = nil) {
        
        let urlString = "\(baseURL)\(AOLEndPoints.Types.rawValue)"
        let headers = requestHeader(user: user)
        
        Alamofire.request(urlString).responseJSON { (response) in
            
            let result = response.result
            
            switch result{
            case .success:
                
                let json = JSON(response.data)
                
                if let programs = json[ProgramServerKey.types.rawValue].array {
                    
                    let context = self.persistentService.managedObjectContext
                    var programArray = [Program]()
                    
                    for programJson in programs{
                        if let programDic = programJson.dictionary{
                            
                            let program = Program.programTypeWithId(programTypeId: programJson["id"].intValue, context: context)
                            program.updateWithDictionary(json: programDic)
                            if program.isActive == true{
                                programArray.append(program)
                            }
                        }
                    }
                    
                    do {
                        try context.save()
                    } catch let error as NSError {
                        DispatchQueue.main.async {
                            print(error)
                        }
                    }
                    
                    DispatchQueue.main.async {
                        success?(programArray, json)
                    }
            }

            case .failure(let error):
                failure?(error as NSError)
            }
        }
    }
    
    func getSubProgramTypes(user: User?, failure: FailureClosure? = nil, success: ((_ savedObject: Any, _ json: JSON) -> Void)? = nil) {
        
        let urlString = "\(baseURL)\(AOLEndPoints.SubTypes.rawValue)"
        let headers = requestHeader(user: user)
        
        Alamofire.request(urlString).responseJSON { (response) in
            
            let result = response.result
            
            switch result{
            case .success:
                
                let json = JSON(response.data)
                
                if let subPrograms = json[SubProgramServerKey.subtypes.rawValue].array {
                    
                    let context = self.persistentService.managedObjectContext
                    var subProgramArray = [SubProgram]()
                    
                    for subProgramJson in subPrograms{
                        if let subProgramDic = subProgramJson.dictionary{
                            
                            let subProgram = SubProgram.subProgramTypeWithId(subProgramTypeId: subProgramJson["id"].intValue, context: context)
                            subProgram.updateWithDictionary(json: subProgramDic)
                            subProgram.program = Program.programTypeWithId(programTypeId: Int(subProgram.typeId), context: context)
                            subProgramArray.append(subProgram)
                            if subProgram.isActive == true{
                                subProgramArray.append(subProgram)
                            }
                        }
                    }
                    
                    do {
                        try context.save()
                    } catch let error as NSError {
                        DispatchQueue.main.async {
                            print(error)
                        }
                    }
                    
                    DispatchQueue.main.async {
                        success?(subProgramArray, json)
                    }
                }
                
            case .failure(let error):
                failure?(error as NSError)
            }
        }
    }

}
