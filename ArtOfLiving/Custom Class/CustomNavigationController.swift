//
//  CustomNavigationController.swift
//  ArtOfLiving
//
//  Created by Dhayanithi on 17/01/18.
//  Copyright © 2018 Kuya. All rights reserved.
//

import UIKit

class CustomNavigationController: NSObject, UIViewControllerAnimatedTransitioning {
    
    var reverse = false
    var duration = 0.1

    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }
    
    //fade in / out
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
//        if Menus.selectedMenu.rawValue == Menus.Programs.rawValue{
//            fadeTransitionAnimation(transitionContext: transitionContext)
//        }else if Menus.selectedMenu.rawValue == Menus.MyJourney.rawValue{
//            cubicTransitionAnimation(transitionContext: transitionContext)
//        }else if Menus.selectedMenu.rawValue == Menus.Accommodation.rawValue{
//            pageTransitionAnimation(transitionContext: transitionContext)
//        }else if Menus.selectedMenu.rawValue == Menus.Access.rawValue{
//            fadeTransitionAnimation(transitionContext: transitionContext)
//        }else if Menus.selectedMenu.rawValue == Menus.ePay.rawValue{
//            cubicTransitionAnimation(transitionContext: transitionContext)
//        }else if Menus.selectedMenu.rawValue == Menus.Map.rawValue{
//            pageTransitionAnimation(transitionContext: transitionContext)
//        }else if Menus.selectedMenu.rawValue == Menus.Seva.rawValue{
//            fadeTransitionAnimation(transitionContext: transitionContext)
//        }else if Menus.selectedMenu.rawValue == Menus.MeetGuruji.rawValue{
//            cubicTransitionAnimation(transitionContext: transitionContext)
//        }else if Menus.selectedMenu.rawValue == Menus.Panchakarma.rawValue{
//            pageTransitionAnimation(transitionContext: transitionContext)
//        }else if Menus.selectedMenu.rawValue == Menus.Shuttle.rawValue{
//            cubicTransitionAnimation(transitionContext: transitionContext)
//        }else {
//            fadeTransitionAnimation(transitionContext: transitionContext)
//        }
        
        fadeTransitionAnimation(transitionContext: transitionContext)
    }
    
    //cubic transition
    func fadeTransitionAnimation(transitionContext: UIViewControllerContextTransitioning){
        
        let containerView = transitionContext.containerView
        let fromView = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)?.view
        let toView = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)?.view
        
        containerView.addSubview(toView!)
        
        fromView?.alpha = 1.0
        toView?.alpha = 0.0
        
        UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: {
            fromView?.alpha = 0.0
            toView?.alpha = 1.0
        }) { (completed) in
            if (transitionContext.transitionWasCancelled) {
                toView?.removeFromSuperview()
            } else {
                fromView?.removeFromSuperview()
            }
            
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
    }
    
    func cubicTransitionAnimation(transitionContext: UIViewControllerContextTransitioning){
        
        let containerView = transitionContext.containerView
        let fromView = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)?.view
        let toView = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)?.view
        
        let direction : CGFloat = reverse == true ? -1 : 1
        let transformPoint : CGFloat = -0.005
        
        toView?.layer.anchorPoint = CGPoint.init(x: direction == 1 ? 0 : 1, y: 0.5)
        fromView?.layer.anchorPoint = CGPoint.init(x: direction == 1 ? 1 : 0, y: 0.5)
        
        var fromViewTransition = CATransform3DMakeRotation(CGFloat(direction) * CGFloat(Double.pi / 2), 0.0, 1.0, 0.0)
        var toViewTransition = CATransform3DMakeRotation(CGFloat(-direction) * CGFloat(Double.pi / 2), 0.0, 1.0, 0.0)
        
        fromViewTransition.m34 = transformPoint
        toViewTransition.m34 = transformPoint
        
        containerView.transform = CGAffineTransform.init(translationX: direction * containerView.frame.size.width / 2.0, y: 0.0)
        toView?.layer.transform = toViewTransition
        containerView.addSubview(toView!)
        
        UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: {
            containerView.transform = CGAffineTransform.init(translationX: -direction * containerView.frame.size.width / 2.0, y: 0.0)
            fromView?.layer.transform = fromViewTransition
            toView?.layer.transform = CATransform3DIdentity
        }) { (completed) in
            containerView.transform = CGAffineTransform.identity
            fromView?.layer.transform = CATransform3DIdentity
            toView?.layer.transform = CATransform3DIdentity
            
            fromView?.layer.anchorPoint = CGPoint.init(x: 0.5, y: 0.5)
            toView?.layer.anchorPoint = CGPoint.init(x: 0.5, y: 0.5)
            
            if (transitionContext.transitionWasCancelled) {
                toView?.removeFromSuperview()
            } else {
                fromView?.removeFromSuperview()
            }
            
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
    }
    
    func pageTransitionAnimation(transitionContext: UIViewControllerContextTransitioning){
        
        let containerView = transitionContext.containerView
        let fromView = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)?.view
        let toView = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)?.view
        
        let direction : CGFloat = reverse == true ? -1 : 1
        let transformPoint : CGFloat = -0.005
        
        toView?.layer.anchorPoint = CGPoint.init(x: direction == 1 ? 0 : 1, y: 0.5)
        fromView?.layer.anchorPoint = CGPoint.init(x: direction == 1 ? 1 : 0, y: 0.5)
        
        var fromViewTransition = CATransform3DMakeRotation(CGFloat(direction) * CGFloat(Double.pi / 2), 0.0, 1.0, 0.0)
        var toViewTransition = CATransform3DMakeRotation(CGFloat(-direction) * CGFloat(Double.pi / 2), 0.0, 1.0, 0.0)
        
        containerView.transform = CGAffineTransform.init(translationX: direction * containerView.frame.size.width / 2.0, y: 0.0)
        toView?.layer.transform = toViewTransition
        containerView.addSubview(toView!)
        
        UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: {
            containerView.transform = CGAffineTransform.init(translationX: -direction * containerView.frame.size.width / 2.0, y: 0.0)
            fromView?.layer.transform = fromViewTransition
            toView?.layer.transform = CATransform3DIdentity
        }) { (completed) in
            containerView.transform = CGAffineTransform.identity
            fromView?.layer.transform = CATransform3DIdentity
            toView?.layer.transform = CATransform3DIdentity
            
            fromView?.layer.anchorPoint = CGPoint.init(x: 0.5, y: 0.5)
            toView?.layer.anchorPoint = CGPoint.init(x: 0.5, y: 0.5)
            
            if (transitionContext.transitionWasCancelled) {
                toView?.removeFromSuperview()
            } else {
                fromView?.removeFromSuperview()
            }
            
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
    }
}
