//
//  Themes.swift
//  ArtOfLiving
//
//  Created by Dhayanithi on 10/02/18.
//  Copyright © 2018 Kuya. All rights reserved.
//

import UIKit

let Default_Time = 10
let Split_Hours = 8

enum ThemeImageType : Int{
    case Ashram = 0
    case Guruji
}

enum Theme : Int{
    case EarlyMorning = 0
    case Noon
    case Dawn
    
    static var selectedTheme = Theme.EarlyMorning
    
    subscript(index: Int) -> Theme{
        switch index {
        case Theme.EarlyMorning.rawValue:
            return Theme.EarlyMorning
        case Theme.Noon.rawValue:
            return Theme.Noon
        case Theme.Dawn.rawValue:
            return Theme.Dawn
            
        default:
            return Theme.EarlyMorning
        }
    }
    
    static func getImageName(index: Int) -> String{
        
        switch index {
        case Theme.EarlyMorning.rawValue:
            return "early_morning"
        case Theme.Noon.rawValue:
            return "noon"
        case Theme.Dawn.rawValue:
            return "dawn"
            
        default:
            return "noon"
        }
    }
    
}

class Themes: NSObject {
    
    private static var themeImageType : ThemeImageType = ThemeImageType.Ashram
    
    static func currentTheme() -> Theme {
        
        let hour = getCurrentHour()
        
        if hour / Split_Hours == 1 {
            return Theme.EarlyMorning
        }
        else if hour / Split_Hours == 2 {
            return Theme.Noon
        }
        else {
            return Theme.Dawn
        }
    }
    
    static func getCurrentHour() -> Int{
        
        let date = Date()
        let calender = Calendar.current
        let components = calender.dateComponents([.year,.month,.day,.hour,.minute,.second], from: date)
        
        return components.hour ?? Default_Time
    }
    
    static func image() -> UIImage {
        
        switch currentTheme() {
            case Theme.EarlyMorning:
                return (themeImageType == ThemeImageType.Ashram) ? UIImage.init(named: Theme.getImageName(index: Theme.EarlyMorning.rawValue))! : UIImage.init(named: "guruji")!
            case Theme.Noon:
                return (themeImageType == ThemeImageType.Ashram) ? UIImage.init(named: Theme.getImageName(index: Theme.Noon.rawValue))! : UIImage.init(named: "guruji")!
            case Theme.Dawn:
                return (themeImageType == ThemeImageType.Ashram) ? UIImage.init(named: Theme.getImageName(index: Theme.Dawn.rawValue))! : UIImage.init(named: "guruji")!

            default:
                return (themeImageType == ThemeImageType.Ashram) ? UIImage.init(named: Theme.getImageName(index: Theme.EarlyMorning.rawValue))! : UIImage.init(named: "guruji")!
        }
    }
    
    private static func getThemeTypeImage() -> UIImage {
        
        switch themeImageType {
        case ThemeImageType.Ashram:
            return UIImage.init()
        case ThemeImageType.Guruji:
            return UIImage.init()

        default:
            return UIImage.init()
        }
    }
}
