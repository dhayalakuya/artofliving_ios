//
//  AOLLayoutAttributesAnimator.swift
//  ArtOfLiving
//
//  Created by Dhayanithi on 15/02/18.
//  Copyright © 2018 Kuya. All rights reserved.
//

import UIKit

public protocol AOLLayoutAttributesAnimator {
    func animate(collectionView: UICollectionView, attributes: AOLAnimatedCollectionViewLayoutAttributes)
}
