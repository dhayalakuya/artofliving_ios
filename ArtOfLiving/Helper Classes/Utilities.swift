//
//  Utilities.swift
//  ArtOfLiving
//
//  Created by Dhayanithi on 18/01/18.
//  Copyright © 2018 Kuya. All rights reserved.
//

import UIKit

enum DeviceNames : String{
    case iPhone5
    case iPhone6To8
    case iPhone6STo8S
    case iPhoneX
}

class Utilities: NSObject {

    class func getDeviceName() -> DeviceNames{
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                return DeviceNames.iPhone5
            case 1334:
                return DeviceNames.iPhone6To8
            case 2208:
                return DeviceNames.iPhone6STo8S
            case 2436:
                return DeviceNames.iPhoneX
            default:
                return DeviceNames.iPhone6To8
            }
        }
        else{
            return DeviceNames.iPhone6To8
        }
    }
}

extension UIView {
    
    func image() -> UIImage {
        
        UIGraphicsBeginImageContext(self.frame.size)
        self.layer.render(in:UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image ?? UIImage.init()
    }

    
}
