//
//  VisualCustomCollectionViewLayout.swift
//  Test
//
//  Created by Dhayanithi on 05/01/18.
//  Copyright © 2018 Dhayanithi. All rights reserved.
//

import UIKit

struct VisualCustomCollectionViewLayoutConstants{
    struct Cell {
        static let standardHeight: CGFloat = 100
        static let featuredHeight: CGFloat = 400
    }
    
}

class VisualCustomCollectionViewLayout: UICollectionViewFlowLayout {

    let offset : CGFloat = 60
    
    var previousOffset : CGFloat = 0.0
    var currentPage = 0
    
    var cache = [UICollectionViewLayoutAttributes]()
    
    var width: CGFloat{
        get{
            return collectionView!.bounds.size.width - (2 * offset)
        }
    }
    
    var height: CGFloat{
        get{
            return collectionView!.bounds.size.height - 40
        }
    }

    var numberOfItems : Int{
        get{
            return collectionView!.numberOfItems(inSection: 0)
        }
    }
    
    override var collectionViewContentSize: CGSize{
        get{
            let contentWidth = (CGFloat(numberOfItems) * (width + (offset * 1.0)))
            return CGSize(width: contentWidth, height: (self.collectionView?.frame.size.height)!)
        }
    }
    
    override func prepare(){
        cache.removeAll(keepingCapacity: false)

        var frame = CGRect.zero

        for item in 0..<numberOfItems{

            let indexPath = IndexPath.init(item: item, section: 0)
            let attributes = UICollectionViewLayoutAttributes.init(forCellWith: indexPath)
            attributes.zIndex = item

            frame = CGRect.init(x: CGFloat(offset) + (((CGFloat(offset/2)) + width) * CGFloat(item)), y: 20, width: width, height: height)
            attributes.frame = frame
            cache.append(attributes)
        }
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        
        var layoutAttribute = UICollectionViewLayoutAttributes()

        if indexPath.item == currentPage{
            layoutAttribute = cache[currentPage]
            var frame = layoutAttribute.frame
            frame.origin.y = 0
            layoutAttribute.frame = frame
        }
        
        return layoutAttribute
    }
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        
        let itemsCount = collectionView?.dataSource?.collectionView(collectionView ?? UICollectionView(), numberOfItemsInSection: 0)
        
        if let collectionView = collectionView {
            if (previousOffset > (collectionView.contentOffset.x)) && (velocity.x < 0.0) {
                currentPage = max(currentPage - 1, 0)
            }
            else if (previousOffset < collectionView.contentOffset.x) && (velocity.x > 0.0) {
                currentPage = min(currentPage + 1, itemsCount! - 1)
            }
            
            let updatedOffset: CGFloat = rect(for: currentPage).origin.x
            previousOffset = updatedOffset
            return CGPoint(x: updatedOffset, y: proposedContentOffset.y)
        }
        
        return CGPoint(x: 0, y: 0)
    }
    
    func rect(for index: Int) -> CGRect {
        let x: CGFloat = (width + (offset/2)) * CGFloat(index)
        currentPage = index
        previousOffset = x
        return CGRect(x: x, y: 0, width: width, height: height)
    }
        
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        
        var visibleLayoutAttributes = [UICollectionViewLayoutAttributes]()
        
        for attributes in cache {
            if attributes.frame.intersects(rect) {
                visibleLayoutAttributes.append(attributes )
            }
        }
        
        return visibleLayoutAttributes
    }

}
