//
//  CircularMenuTableView.swift
//  Test
//
//  Created by Dhayanithi on 09/01/18.
//  Copyright © 2018 Dhayanithi. All rights reserved.
//

import UIKit

enum CircularMenuTableViewContentAlignment{
    case none
    case left
    case right
};

typealias CircularTableViewCompletionHander = () -> Void

class CircularMenuTableView: UITableView {

    var reloadCompletionHandler : CircularTableViewCompletionHander?
    var layoutCompletionHandler : CircularTableViewCompletionHander?
    
    var isEnableInfiniteScrolling: Bool {
        get{
            return enableInfiniteScrolling
        }
        set(newValue){
            enableInfiniteScrolling = newValue
            
            if (enableInfiniteScrolling) {
                showsVerticalScrollIndicator = false;
            }
        }
    }
    var contentAlignment = CircularMenuTableViewContentAlignment.none
    var radius: CGFloat = 0.0
    var repeatCount: Int = 1
    var numberOfDataSourceRows: Int = 0
    var enableInfiniteScrolling = false
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customIntitialization()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        customIntitialization()
    }
    
    func customIntitialization(){
        
        repeatCount = 3;
        contentAlignment = CircularMenuTableViewContentAlignment.none;
        radius = 0.0;
    
        self.separatorColor = UIColor.clear
        
        enableInfiniteScrolling = true;
        showsVerticalScrollIndicator = false;
        
        self.reloadData()
    }

    func scrollFirstCellToCenter() {
        
        if !(enableInfiniteScrolling || repeatCount < 2) {
            return
        }
        if contentSize.height == 0 || contentSize.height <= bounds.size.height {
            return
        }
        var contentOffset: CGPoint = self.contentOffset
        let contentHeightPerUnit = contentSize.height / CGFloat(repeatCount)
        contentOffset.y = CGFloat(contentHeightPerUnit) - contentCenterInsetTop() + CGFloat((rowHeight / 2))
        self.contentOffset = contentOffset
    }

    func contentCenterInsetTop() -> CGFloat {
        return contentInset.top + (frame.size.height - contentInset.top - contentInset.bottom) / 2.0
    }
    
    func contentCenter() -> CGPoint {
        return CGPoint(x: frame.size.width / 2, y: contentOffset.y + contentCenterInsetTop())
    }
    
    func contentCenterInFrame() -> CGPoint {
        return CGPoint(x: frame.size.width / 2, y: contentCenterInsetTop())
    }
    
    func cells(atRow row: Int) -> [Any] {
        var cells = [AnyHashable]()
        for i in 0..<repeatCount {
            let indexPath = IndexPath(row: row + i * numberOfDataSourceRows, section: 0)
            let cell: UITableViewCell? = cellForRow(at: indexPath)
            if cell != nil {
                cells.append(cell ?? UITableViewCell())
            }
        }
        return cells
    }
    
    func cellAtCenter() -> CircularMenuTableViewCell? {
        return cellForRow(at: indexPathAtCenter()) as? CircularMenuTableViewCell
    }
    
    func cellAtindex(index: Int) -> CircularMenuTableViewCell? {
        return cellForRow(at: IndexPath.init(row: index, section: 0)) as? CircularMenuTableViewCell
    }

    func indexPathAtCenter() -> IndexPath {
        return indexPathForRow(at: contentCenter()) ?? IndexPath(row: 0, section: 0)
    }
    
    func resetContentOffsetIfNeeded() {
        if !(enableInfiniteScrolling || repeatCount < 2) {
            return
        }
        if contentSize.height == 0 || contentSize.height <= bounds.size.height {
            return
        }
        var contentOffset: CGPoint = self.contentOffset
        let contentHeightPerUnit = contentSize.height / CGFloat(repeatCount)
        if contentOffset.y <= 0.0 {
            contentOffset.y = contentHeightPerUnit
        }
        else if contentOffset.y >= (contentSize.height - bounds.size.height) {
            while contentOffset.y > contentHeightPerUnit {
                contentOffset.y -= contentHeightPerUnit
            }
        }
        
        self.contentOffset = contentOffset
    }
    
    func layoutVisibleCells() {
        let indexPaths = indexPathsForVisibleRows!
        let totalVisibleCells: Int! = indexPaths.count
        
        if CircularMenuTableViewContentAlignment.none != contentAlignment {
            
            let contentCenterInFrame = self.contentCenterInFrame
//            let viewHalfHeight = self.frame.size.height / 2.0
            let vRadius = self.frame.size.height
            let hRadius = self.frame.size.width
//            self.rowHeight = 100
            var xRadius = (vRadius < hRadius) ? vRadius : vRadius - (vRadius / getXRadiusOffset()) //CGFloat(hRadius + ((hRadius / 8)))//
            let yRadius =  xRadius - (xRadius / getYRadiusOffset()) //CGFloat(hRadius)//(xRadius / 4) * 2.8//viewHalfHeight + self.rowHeight
            
//            var xRadius = CGFloat(474.0)
//            var yRadius = CGFloat(337.0)
            
            if radius > 0 && radius.isFinite == true{
                xRadius = radius
            }
            
            for index in 0..<totalVisibleCells {
                let indexPath: IndexPath? = indexPaths[index]
                
                if let indexPath = indexPath{
                    let cell = cellForRow(at: indexPath)
                    var frame = cell!.frame
                    var rowHeight = self.rowHeight
                    
                    rowHeight = (delegate?.tableView!(self, heightForRowAt: indexPath ))!
                    
                    let y = min(abs(contentCenterInFrame().y - ((frame.origin.y) - contentOffset.y + (rowHeight / 2.0))), yRadius)
                    let angle = CGFloat(asinf(Float(y) / Float(yRadius)))
                    var x = xRadius * CGFloat(cosf(Float(angle)))
                    if contentAlignment == CircularMenuTableViewContentAlignment.left {
                        x = xRadius - x
                    }
                    else {
                        x = x - xRadius / 2
                    }
                    if x.isNaN == false {
//                        frame.origin.x = x + (CGFloat((indexPath.row > 3) ? 3 - (indexPath.row - 3) : (indexPath.row)) *  CGFloat(18.0))
                        frame.origin.x = x
                        cell!.frame = frame
                        cell?.backgroundColor = UIColor.clear
                        cell?.backgroundView?.backgroundColor = UIColor.clear
                    }
                }
            }
        }
        else{
            for index in 0..<totalVisibleCells {
                let indexPath: IndexPath? = indexPaths[index]
                let cell = cellForRow(at: indexPath!)
                var frame: CGRect? = cell?.frame
                frame?.origin.x = 0.0
                cell?.frame = frame!
            }
        }
        
        if let layoutCompletionHandler = layoutCompletionHandler{
            layoutCompletionHandler()
        }
    }
                
    override func layoutSubviews() {
        resetContentOffsetIfNeeded()
        super.layoutSubviews()
        layoutVisibleCells()
    }

    func clearBackground() {
        backgroundColor = UIColor.clear
        separatorStyle = .none
    }
    
    override func reloadData() {
        super.reloadData()
        if let reloadCompletionHandler = reloadCompletionHandler {
            reloadCompletionHandler()
        }
    }
    
    func reloadData(_ completionHander: CircularTableViewCompletionHander) {
        reloadData()
//        if completionHander {
            completionHander()
//        }
    }
    
    func getXRadiusOffset() -> CGFloat {
        
        if Utilities.getDeviceName() == DeviceNames.iPhone5{
            return 5.0
        }
        else if Utilities.getDeviceName() == DeviceNames.iPhone6To8{
            return 4.6
        }else if Utilities.getDeviceName() == DeviceNames.iPhone6STo8S{
            return 5.0
        }
        else if Utilities.getDeviceName() == DeviceNames.iPhoneX{
            return 3.0
        }
        
        return 5.0
    }
    
    func getYRadiusOffset() -> CGFloat {
        
        if Utilities.getDeviceName() == DeviceNames.iPhone5{
            return 4.4
        }
        else if Utilities.getDeviceName() == DeviceNames.iPhone6To8{
            return 4.2
        }
        else if Utilities.getDeviceName() == DeviceNames.iPhone6STo8S{
            return 4.0
        }
        else if Utilities.getDeviceName() == DeviceNames.iPhoneX{
            return 5.0
        }
            
        return 5.0
    }


    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
}
