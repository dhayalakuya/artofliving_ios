//
//  CircularMenuTableViewCell.swift
//  Test
//
//  Created by Dhayanithi on 09/01/18.
//  Copyright © 2018 Dhayanithi. All rights reserved.
//

import UIKit
import Foundation
import CoreGraphics

class CircularMenuTableViewCell: UITableViewCell {

    private var borderColor : UIColor?
    var cellTitleLabel : UILabel?
    private var imageLayer : CALayer?

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = UITableViewCellSelectionStyle.none
        self.contentView.backgroundColor = UIColor.clear
        
        self.clipsToBounds = false
        
        borderColor = UIColor.orange

        //image layer
        imageLayer = CALayer.init()
        if let imageLayer = imageLayer{
            imageLayer.cornerRadius = 8.0;
            imageLayer.borderWidth = 2.0;
            imageLayer.borderColor = borderColor?.cgColor;
            
            self.contentView.layer.addSublayer(imageLayer)
        }
        
        //title label
        cellTitleLabel = UILabel.init(frame: CGRect.init(x: 44.0, y: 10.0, width: self.contentView.bounds.size.width - 44.0, height: 21.0))
        if let cellTitleLabel = cellTitleLabel{
            self.contentView.addSubview(cellTitleLabel)
            cellTitleLabel.backgroundColor = UIColor.clear;
            cellTitleLabel.textColor       = UIColor.white;
            cellTitleLabel.shadowColor     = UIColor.black;
            cellTitleLabel.shadowOffset    = CGSize.init(width: 1.0, height: 1.0);
            cellTitleLabel.font = UIFont.boldSystemFont(ofSize: getTextFontSize())
        }
    }
    
    func getTextFontSize() -> CGFloat{
        
        if Utilities.getDeviceName() == DeviceNames.iPhone5{
            return 15.0
        }
        else if Utilities.getDeviceName() == DeviceNames.iPhone6To8{
            return 17.0
        }
        else if Utilities.getDeviceName() == DeviceNames.iPhone6STo8S{
            return 18.0
        }
        else if Utilities.getDeviceName() == DeviceNames.iPhoneX{
            return 18.0
        }
        
        return 15.0
    }

    func getImageHeight() -> CGFloat{
        
        if Utilities.getDeviceName() == DeviceNames.iPhone5{
            return 30.0
        }
        else if Utilities.getDeviceName() == DeviceNames.iPhone6To8{
            return 35.0
        }
        else if Utilities.getDeviceName() == DeviceNames.iPhone6STo8S{
            return 35.0
        }
        else if Utilities.getDeviceName() == DeviceNames.iPhoneX{
            return 35.0
        }
        
        return 30.0
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let imageY             = 5.0;
        var heightOfImageLayer = CGFloat(self.bounds.size.height) - CGFloat(imageY) * 6.0
        heightOfImageLayer       = getImageHeight() //CGFloat(floor(heightOfImageLayer))
        imageLayer!.cornerRadius = heightOfImageLayer / 2.0
        var point = CGPoint.init(x: CGFloat(8.0), y: CGFloat(imageY))
        var size = CGSize.init(width: CGFloat(heightOfImageLayer), height: CGFloat(heightOfImageLayer))
        
        if self.cellTitleLabel?.text?.isEmpty == true{
            imageLayer!.frame = CGRect.zero
        }
        else{
            imageLayer!.frame = CGRect.init(origin: point, size: size)
        }
    
        point = CGPoint.init(x: CGFloat(CGFloat(heightOfImageLayer) + 20.0), y: CGFloat(floorf(Float(CGFloat(heightOfImageLayer) / 2.0 - (21.0 / 2.0))) + 4.0))
        size = CGSize.init(width: CGFloat(self.contentView.bounds.size.width - heightOfImageLayer + 10.0), height: CGFloat(21.0))
        cellTitleLabel!.frame = CGRect.init(origin: point, size: size)
    }
    
    func setCellTitle(title: String){
        cellTitleLabel?.text = title
    }
    
    func setImage(image: UIImage)
    {
        CATransaction.begin()
        CATransaction.setAnimationDuration(0.0)
        imageLayer?.contents = image.cgImage
        CATransaction.commit()
    }
    
    func setBorderColor(color: UIColor)
    {
        borderColor            = color;
        imageLayer?.borderColor = color.cgColor;
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

//        imageLayer!.borderColor  = selected ? UIColor.orange.cgColor : borderColor?.cgColor
//        cellTitleLabel!.textColor = selected ? UIColor.white : borderColor
    }
}
