//
//  AOLActivityIndicator.swift
//  ArtOfLiving
//
//  Created by Dhayanithi on 27/01/18.
//  Copyright © 2018 Kuya. All rights reserved.
//

import UIKit

class AOLActivityIndicatorView: UIView {

    private var greyView : UIView!
    var navigationControllerFrame = CGRect.zero
    
    convenience init(){
        self.init(frame: CGRect.zero)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.frame = frame
        intialization()
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)!
        
        intialization()
    }
    
    func intialization(){
        
//        self.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
//        self.color = UIColor.init(red: 226.0/255.0, green: 111.0/255.0, blue: 35.0/255.0, alpha: 1.0)
//        self.transform = CGAffineTransform.init(scaleX: 2.0, y: 2.0)
//
//        //greyview
//        greyView = UIView.init(frame: CGRect.init(x: 0, y: self.frame.origin.y, width: self.frame.size.width, height: self.frame.size.height))
//        greyView.isHidden = true
//        greyView.backgroundColor = UIColor.init(white: 0, alpha: 0.25)
//        greyView.isUserInteractionEnabled = false
//
//        self.hidesWhenStopped = true
    }
    
    func startActivity(){
        greyView.isHidden = false
//        self.startAnimating()
    }

    func stopActivity(){
        greyView.isHidden = true
//        self.stopAnimating()
    }
    
    var parentView : UIView?{
        didSet{

            if let parentView = parentView{
            
                if self.superview != nil{
                    self.removeFromSuperview()
                    greyView.removeFromSuperview()
                }
                
                greyView.frame = CGRect.init(x: 0, y: self.frame.origin.y, width: parentView.frame.size.width, height: parentView.frame.size.height)
                parentView.addSubview(greyView)
            }
        }
    }
    
    override func layoutSubviews() {
        if let parentView = parentView{
            setActivityIndicator()
        }
    }
    
    func setActivityIndicator(){
        
        var frame = self.frame
        frame.origin.x = (parentView!.frame.size.width - 30) / 2.0
        frame.origin.y = (parentView!.frame.size.height - 30) / 2.0
        self.frame = frame
        self.sendSubview(toBack: greyView)
    }
}
