//
//  CustomLoader.swift
//  ArtOfLiving
//
//  Created by Dhayanithi on 31/01/18.
//  Copyright © 2018 Kuya. All rights reserved.
//

import UIKit

class CustomLoader: UIView {
    
    var firstColor : UIColor = UIColor.yellow
    var secondColor : UIColor = UIColor.orange
    var lineWidth : CGFloat = 10.0
    var baseColor : UIColor = UIColor.yellow
    var duration : Double = 0.1
    
    // MARK: Initalization methods
    convenience init(){
        self.init(frame: CGRect.zero)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.frame = frame
        intialization()
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)!
        
        intialization()
    }
    
    func intialization(){
        
        self.backgroundColor = UIColor.clear
    }
    
    // MARK: Drawing methods
    override func layoutSubviews() {
        super.layoutSubviews()
//        let dimension = self.frame.size.width
//
//        let donut = UIBezierPath.init(ovalIn: CGRect.init(x: lineWidth / 2, y: lineWidth / 2, width: CGFloat(dimension - lineWidth), height: CGFloat(dimension - lineWidth)))
//
//        let baseTrack = CAShapeLayer()
//        baseTrack.path = donut.cgPath
//        baseTrack.lineWidth = lineWidth
//        baseTrack.fillColor = UIColor.clear.cgColor
//        baseTrack.strokeStart = 0.0
//        baseTrack.strokeEnd = 1.0
//        baseTrack.strokeColor = baseColor.cgColor
//        baseTrack.lineCap = kCALineCapButt
//        layer.addSublayer(baseTrack)
//
//        let clipView = UIView()
//        clipView.frame = bounds
//        self.addSubview(clipView)
//
//        let rotateView = UIView()
//        rotateView.frame = self.bounds
//        clipView.addSubview(rotateView)
//
//        let radialGradient = UIImageView.init()
//        radialGradient.frame = self.bounds
//        rotateView.addSubview(radialGradient)

    }
    
    override func draw(_ rect: CGRect) {
        
        super.draw(rect)
        
        var endAngle : CGFloat = 0.0
        let maxAngle : CGFloat = 100.0
        
        let gradients = 255
        
        let center = CGPoint.init(x: self.bounds.width / 2.0, y: self.bounds.height / 2.0)
        let radius = (self.bounds.width / 2.0) - 10
        
        let startAngle : CGFloat = CGFloat(maxAngle - endAngle)
        var angle = startAngle

        var spectrumColours = [AnyHashable]()
        
        var fR: CGFloat = 0.0
        var fG: CGFloat = 0.0
        var fB: CGFloat = 0.0
        
        var tR: CGFloat = 0.0
        var tG: CGFloat = 0.0
        var tB: CGFloat = 0.0
        
        firstColor.getRed(&fR, green: &fG, blue: &fB, alpha: nil)
        secondColor.getRed(&tR, green: &tG, blue: &tB, alpha: nil)
        
        let numberOfColours: Int = 360
        let dR: CGFloat = (tR - fR) / CGFloat(numberOfColours - 1)
        let dG: CGFloat = (tG - fG) / CGFloat(numberOfColours - 1)
        let dB: CGFloat = (tB - fB) / CGFloat(numberOfColours - 1)
        
        //gradient colors
        for n in 0..<numberOfColours {
            spectrumColours.append(UIColor.init(red: CGFloat((fR + CGFloat(n) * dR)), green: CGFloat((fG + CGFloat(n) * dG)), blue: CGFloat((fB + CGFloat(n) * dB)), alpha: 1.0))
        }
        
        for i in 1 ... gradients {
            let percent = CGFloat(i) / CGFloat(gradients)
            endAngle = CGFloat(startAngle) - CGFloat(percent) * CGFloat(maxAngle)
            let path = UIBezierPath()
            path.lineWidth = lineWidth
            path.lineCapStyle = .butt
            path.addArc(withCenter: center, radius: radius, startAngle: (maxAngle - endAngle) / 100 * CGFloat(Double.pi) * 2 - CGFloat(Double.pi), endAngle: CGFloat((maxAngle - angle) / 100 * CGFloat(Double.pi) * 2 - CGFloat(Double.pi)), clockwise: false)
            
            let color = spectrumColours[i+100] as! UIColor
            color.withAlphaComponent(percent).setStroke()
            
            path.stroke()
            
            UIColor.clear.setFill()
            angle = CGFloat(endAngle)
        }
    }
    
    func rotateBorder(view: UIView, duration: Double){
        UIView.animate(withDuration: duration, delay: 0.0, options: [.curveLinear], animations: {
            view.transform = view.transform.rotated(by: CGFloat(-Double.pi))
        }) { (finished) in
            self.rotateBorder(view: view, duration: duration)
        }
    }
}
