//
//  CustomLoadingView.swift
//  ArtOfLiving
//
//  Created by Dhayanithi on 29/01/18.
//  Copyright © 2018 Kuya. All rights reserved.
//

import UIKit

class CustomLoadingView: UIView {
    
    var index = 0
    
    var viewOneFrame : CGRect = CGRect.init(x: 20, y: 50, width: 50, height: 50)
    var viewTwoFrame : CGRect = CGRect.init(x: 50, y: 50, width: 50, height: 50)
    var viewThreeFrame : CGRect = CGRect.init(x: 20, y: 70, width: 50, height: 50)
    var viewFourFrame : CGRect = CGRect.init(x: 70, y: 70, width: 50, height: 50)
    
    let viewOneAngle : Double = 15.0
    let viewTwoAngle : Double = 10.0
    let viewThreeAngle : Double = 90.0
    let viewFourAngle : Double = 180.0
    
    var viewOne = UIView()
    let viewTwo = UIView()
    let viewThree = UIView()
    let viewFour = UIView()
    
    let viewGradient = UIView()

    let loaderView = CustomLoader()
    
    let animationDuration  =  0.5
    let animationDelay = 0.0
    
    convenience init(){
        self.init(frame: CGRect.zero)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.frame = frame
        intialization()
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)!
        
        intialization()
    }
    
    func intialization(){
        
        let objectWidth : CGFloat = 50.0
        let center : CGFloat = bounds.width / 2.0
        let interItemOffset : CGFloat = -5.0

        viewOneFrame = CGRect.init(x: ((center - objectWidth)) - interItemOffset, y: center - ((objectWidth)), width: objectWidth, height: objectWidth)

        viewTwoFrame = CGRect.init(x: center + interItemOffset, y: center - ((objectWidth / 2)), width: objectWidth, height: objectWidth)

        viewThreeFrame = CGRect.init(x: (center - objectWidth) - interItemOffset, y: center + (objectWidth / 2), width: objectWidth, height: objectWidth)

        viewFourFrame = CGRect.init(x: center - ((objectWidth / 2)), y: center + (objectWidth), width: objectWidth, height: objectWidth)

        if loaderView.superview == nil{
            let loaderView = CustomLoader.init(frame: CGRect.init(x: bounds.origin.x, y: bounds.origin.y, width: bounds.width, height: bounds.height))
            self.addSubview(loaderView)
        }
    }
    
    func drawCubicImageLayer(frame: CGRect, startAngle: CGFloat, rotateAngle: Double, clockwise: Bool) {
        
        var radius = 12.5
        
        var endAngle = CGFloat(0.0)
        if clockwise == true{
            endAngle = (startAngle + 75) / 100 * CGFloat(Double.pi) * 2 - CGFloat(Double.pi)
        }
        else{
            endAngle = (startAngle - 75) / 100 * CGFloat(Double.pi) * 2 - CGFloat(Double.pi)
        }

        
        let startAngle = startAngle / 100 * CGFloat(Double.pi) * 2 - CGFloat(Double.pi)
        
        
        let path = UIBezierPath()
        //        path.move(to: CGPoint.init(x: 30.0, y: 30.0))
        //        path.addArc(withCenter: CGPoint.init(x: 30.0, y: 30.0), radius: CGFloat(radius), startAngle: startAngle, endAngle: endAngle, clockwise: true)
        
        
        let layer = CAShapeLayer()
        path.move(to: CGPoint.init(x: 25, y: 25.0))
        path.addArc(withCenter: CGPoint.init(x: 25.0, y: 25.0), radius: CGFloat(radius), startAngle: startAngle, endAngle: endAngle, clockwise: clockwise)
        
        layer.path = path.cgPath
        
        layer.fillColor = UIColor.red.cgColor
        
        if index == 0{
            viewOne.backgroundColor = UIColor.clear
            layer.fillColor = UIColor.orange.cgColor

            //            layer.fillColor = UIColor.gray.cgColor
            
            viewOne.frame = frame
            viewOne.layer.addSublayer(layer)
            viewOne.clipsToBounds = false
//            viewOne.transform = CGAffineTransform.init(rotationAngle: CGFloat((rotateAngle * Double.pi) / 180.0))
            
            self.addSubview(viewOne)
            
            let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
            rotateAnimation.fromValue = 0.0
            rotateAnimation.toValue = CGFloat(-(Double.pi + 5))
            rotateAnimation.duration = self.animationDuration

            viewOne.layer.add(rotateAnimation, forKey: "rotateAnimation")

            UIView.animate(withDuration: self.animationDuration, animations: {
                self.viewOne.frame = self.viewTwoFrame
//                self.viewOne.transform = CGAffineTransform.init(rotationAngle: CGFloat((110.0 * Double.pi) / 180.0))

            }, completion: { (finished) in
//                self.viewOne.transform = CGAffineTransform.init(rotationAngle: CGFloat((self.viewTwoAngle * Double.pi) / 180.0))

                

//                self.viewOneAnimation()
//
//                                UIView.animate(withDuration: 10.0, delay: 0.1, options: [UIViewAnimationOptions.repeat], animations: {
//                                    self.viewOneAnimation()
//                                }, completion: { (finished) in
//                //                    self.viewOne.frame = self.viewOneFrame
//                                })

            })
        }
        
        if index == 1{
            viewTwo.backgroundColor = UIColor.clear
            
            viewTwo.frame = frame
            viewTwo.layer.addSublayer(layer)
            viewTwo.clipsToBounds = false
//            viewTwo.transform = CGAffineTransform.init(rotationAngle: CGFloat((rotateAngle * Double.pi) / 180.0))
            
            layer.fillColor = UIColor.orange.cgColor
            self.addSubview(viewTwo)

            let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
            rotateAnimation.fromValue = 0.0
            rotateAnimation.toValue = CGFloat(-(Double.pi + 4))
            rotateAnimation.duration = self.animationDuration


            viewTwo.layer.add(rotateAnimation, forKey: "rotateAnimation")

            UIView.animate(withDuration: self.animationDuration, animations: {
                self.viewTwo.frame = self.viewThreeFrame

            }, completion: { (finished) in
//                self.viewTwo.transform = CGAffineTransform.init(rotationAngle: CGFloat((self.viewThreeAngle * Double.pi) / 180.0))
//
//                self.viewTwoAnimation()

                //                UIView.animate(withDuration: 10.0, delay: 0.1, options: [UIViewAnimationOptions.repeat], animations: {
                //                    self.viewTwoAnimation()
                //                }, completion: { (finished) in
                ////                        self.viewTwo.frame = self.viewTwoFrame
                //                })
            })
        }
        
        if index == 2{
            //            view.backgroundColor = UIColor.green
            
            viewThree.frame = frame
            viewThree.backgroundColor = UIColor.clear
            viewThree.layer.addSublayer(layer)
            viewThree.clipsToBounds = false
//            viewThree.transform = CGAffineTransform.init(rotationAngle: CGFloat((rotateAngle * Double.pi) / 180.0))
            self.addSubview(viewThree)

            layer.fillColor = UIColor.yellow.cgColor

            let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
            rotateAnimation.fromValue = 0.0
            rotateAnimation.toValue = CGFloat(-(Double.pi + 4))
            rotateAnimation.duration = self.animationDuration


            viewThree.layer.add(rotateAnimation, forKey: "rotateAnimation")

            UIView.animate(withDuration: self.animationDuration, animations: {
                self.viewThree.frame = self.viewThreeFrame

            }, completion: { (finished) in
//                self.viewThree.transform = CGAffineTransform.init(rotationAngle: CGFloat((self.viewFourAngle * Double.pi) / 180.0))
//                self.viewThreeAnimation()

                //                UIView.animate(withDuration: 10.0, delay: 0.1, options: [UIViewAnimationOptions.repeat], animations: {
                //                    self.viewThreeAnimation()
                //                }, completion: { (finished) in
                ////                        self.viewThree.frame = self.viewThreeFrame
                //                })

            })
        }
        
        if index == 3{
            //            view.backgroundColor = UIColor.green
            
            viewFour.frame = frame
            viewFour.backgroundColor = UIColor.clear
            viewFour.layer.addSublayer(layer)
            viewFour.clipsToBounds = false
//            viewFour.transform = CGAffineTransform.init(rotationAngle: CGFloat((rotateAngle * Double.pi) / 180.0))
            self.addSubview(viewFour)

            layer.fillColor = UIColor.yellow.cgColor

            let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
            rotateAnimation.fromValue = 0.0
            rotateAnimation.toValue = CGFloat(-(Double.pi + 4))
            rotateAnimation.duration = self.animationDuration


            viewFour.layer.add(rotateAnimation, forKey: "rotateAnimation")

            UIView.animate(withDuration: self.animationDuration, animations: {
                self.viewFour.frame = self.viewFourFrame

            }, completion: { (finished) in
//                self.viewFour.transform = CGAffineTransform.init(rotationAngle: CGFloat((self.viewOneAngle * Double.pi) / 180.0))
//
//                self.viewFourAnimation()
                //                UIView.animate(withDuration: 10.0, delay: 0.1, options: [UIViewAnimationOptions.repeat], animations: {
                //                    self.viewFourAnimation()
                //                }, completion: { (finished) in
                ////                    self.viewFour.frame = self.viewFourFrame
                //                })

            })
        }
        
        //            UIView.animate(withDuration: 10.0) {
        //
        //                UIView.animate(withDuration: 10.0, animations: {
        //                    view.frame = CGRect.init(x: 20, y: 20, width: 50, height: 50)
        //
        //                }, completion: { (finished) in
        //                    view.transform = CGAffineTransform.init(rotationAngle: CGFloat((60.0 * Double.pi) / 180.0))
        //
        //                })
        //
        ////                view.transform = CGAffineTransform.init(rotationAngle: CGFloat((60.0 * Double.pi) / 180.0))
        ////                view.frame = CGRect.init(x: 20, y: 20, width: 50, height: 50)
        //                //            self.transform = CGAffineTransform.init(rotationAngle: CGFloat(CGFloat((Double.pi))))
        //                //            layer.setAffineTransform(CGAffineTransform.init(rotationAngle: CGFloat((120.0 * Double.pi) / 180.0)))
        //
        //            }
        //            UIView.animate(withDuration: 1.0, delay: 10.0, options: .curveLinear, animations: {
        ////                view.transform = CGAffineTransform.init(rotationAngle: CGFloat((60.0 * Double.pi) / 180.0))
        //                view.frame = CGRect.init(x: 20, y: 20, width: 50, height: 50)
        //            }) { finished in
        //            }
        //        }
        
        //            UIView.animate(withDuration: 10.0) {
        //
        //                view.transform = CGAffineTransform.init(rotationAngle: CGFloat((60.0 * Double.pi) / 180.0))
        //                view.frame = CGRect.init(x: 20, y: 20, width: 50, height: 50)
        //                //            self.transform = CGAffineTransform.init(rotationAngle: CGFloat(CGFloat((Double.pi))))
        //                //            layer.setAffineTransform(CGAffineTransform.init(rotationAngle: CGFloat((120.0 * Double.pi) / 180.0)))
        //
        //            }
        
        //        }
        
        //        if index == 2{
        //
        //            UIView.animate(withDuration: 10.0) {
        //                view.transform = CGAffineTransform.init(rotationAngle: CGFloat((0 * Double.pi) / 180.0)).translatedBy(x: -30, y: 0)
        //                //            self.transform = CGAffineTransform.init(rotationAngle: CGFloat(CGFloat((Double.pi))))
        //                //            layer.setAffineTransform(CGAffineTransform.init(rotationAngle: CGFloat((120.0 * Double.pi) / 180.0)))
        //
        //            }
        //
        //        }
        //
        //        if index == 1{
        //            view.backgroundColor = UIColor.white
        //            let path1 = UIBezierPath()
        //            path1.move(to: CGPoint(x: 80.0,y: 2.0))
        //
        //
        //            path1.addQuadCurve(to: CGPoint(x: 20.0, y: 0.0), controlPoint: CGPoint(x: 20, y: 10))
        //
        //            let animation = CAKeyframeAnimation(keyPath: "position")
        //            animation.path = path1.cgPath
        //
        //            animation.repeatCount = 0
        //            animation.duration = 5.0
        //
        //            view.layer.add(animation, forKey:"position along path")
        
        
        //            UIView.animate(withDuration: 10.0) {
        //                view.transform = CGAffineTransform.init(rotationAngle: CGFloat((0 * Double.pi) / 180.0)).translatedBy(x: 0, y: 30)
        //                //            self.transform = CGAffineTransform.init(rotationAngle: CGFloat(CGFloat((Double.pi))))
        //                //            layer.setAffineTransform(CGAffineTransform.init(rotationAngle: CGFloat((120.0 * Double.pi) / 180.0)))
        //
        //            }
        
        //        }
        
        
        
        index = index + 1
    }
    
    
    
    @objc func viewOneAnimation(){
    
        self.viewOne.frame = viewOneFrame
        viewOne.backgroundColor = UIColor.clear
        //        viewOne.layer.addSublayer(layer)
        viewOne.clipsToBounds = false
        //        viewOne.transform = CGAffineTransform.init(rotationAngle: CGFloat((viewOneAngle * Double.pi) / 180.0))
    
        //        self.addSubview(viewOne)
    
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat(-(Double.pi + 4))
        rotateAnimation.duration = self.animationDuration
    
        viewOne.layer.add(rotateAnimation, forKey: "rotateAnimation")
    
        UIView.animate(withDuration: self.animationDuration, animations: {
            self.viewOne.frame = self.viewTwoFrame
            //            self.viewOne.transform = CGAffineTransform.init(rotationAngle: CGFloat((110.0 * Double.pi) / 180.0))
    
        }, completion: { (finished) in
            //            self.viewTwo.transform = CGAffineTransform.init(rotationAngle: CGFloat((90.0 * Double.pi) / 180.0))
    
            self.viewOne.frame = self.viewOneFrame
    
            self.perform(#selector(self.viewOneAnimation), with: self, afterDelay: self.animationDelay)
            //            self.viewOneAnimation()
        })
    }
    
    @objc func viewTwoAnimation(){
    
        viewTwo.frame = viewTwoFrame
        viewTwo.clipsToBounds = false
        //        viewTwo.transform = CGAffineTransform.init(rotationAngle: CGFloat((viewTwoAngle * Double.pi) / 180.0))
    
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat(-(Double.pi + 4))
        rotateAnimation.duration = self.animationDuration
    
        viewTwo.layer.add(rotateAnimation, forKey: "rotateAnimation")
    
        UIView.animate(withDuration: self.animationDuration, animations: {
            self.viewTwo.frame = CGRect.init(x: 50.5, y: 50.5, width: 50, height: 50)
    
        }, completion: { (finished) in
            //            self.viewTwo.transform = CGAffineTransform.init(rotationAngle: CGFloat((25.0 * Double.pi) / 180.0))
            //            self.viewTwo.transform = CGAffineTransform.identity.rotated(by: CGFloat((25.0 * Double.pi) / 180.0))
            self.viewTwo.frame = self.viewTwoFrame
    
            //            self.viewTwoAnimation()
            self.perform(#selector(self.viewTwoAnimation), with: self, afterDelay: self.animationDelay)
    
    
        })
    }
    
    @objc func viewThreeAnimation(){
    
        viewThree.frame = viewThreeFrame
        viewThree.backgroundColor = UIColor.clear
        viewThree.clipsToBounds = false
        //        viewThree.transform = CGAffineTransform.init(rotationAngle: CGFloat((viewThreeAngle * Double.pi) / 180.0))
    
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat(-(Double.pi + 4))
        rotateAnimation.duration = self.animationDuration
    
        viewThree.layer.add(rotateAnimation, forKey: "rotateAnimation")
    
        UIView.animate(withDuration: self.animationDuration, animations: {
            self.viewThree.frame = self.viewFourFrame
    
        }, completion: { (finished) in
            //            self.viewThree.transform = CGAffineTransform.init(rotationAngle: CGFloat((self.viewFourAngle * Double.pi) / 180.0))
    
            self.viewThree.frame = self.viewThreeFrame
            self.perform(#selector(self.viewThreeAnimation), with: self, afterDelay: self.animationDelay)
    
            //            self.viewThreeAnimation()
        })
    }
    
    @objc func viewFourAnimation(){
    
        viewFour.frame = viewFourFrame
        viewFour.backgroundColor = UIColor.clear
        //        viewFour.layer.addSublayer(layer)
        viewFour.clipsToBounds = false
        //        viewFour.transform = CGAffineTransform.init(rotationAngle: CGFloat((viewFourAngle * Double.pi) / 180.0))
    
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat(-(Double.pi + 4))
        rotateAnimation.duration = self.animationDuration
    
        viewFour.layer.add(rotateAnimation, forKey: "rotateAnimation")
    
        UIView.animate(withDuration: self.animationDuration, animations: {
            self.viewFour.frame = self.viewOneFrame
    
        }, completion: { (finished) in
            //            self.viewFour.transform = CGAffineTransform.init(rotationAngle: CGFloat((self.viewOneAngle * Double.pi) / 180.0))
    
            self.viewFour.frame = self.viewFourFrame
    
            //            self.viewFourAnimation()
            self.perform(#selector(self.viewFourAnimation), with: self, afterDelay: self.animationDelay)
    
        })
    }
    
    override func draw(_ rect: CGRect) {
        drawCubicImageLayer(frame: viewOneFrame, startAngle: 45.0, rotateAngle: viewOneAngle, clockwise: false)
        drawCubicImageLayer(frame: viewTwoFrame, startAngle: 5.0, rotateAngle: viewTwoAngle, clockwise: true)
        drawCubicImageLayer(frame: viewThreeFrame, startAngle: 25.0, rotateAngle: viewThreeAngle, clockwise: false)
        drawCubicImageLayer(frame: viewFourFrame, startAngle: 25.0, rotateAngle: viewFourAngle, clockwise: true)
    }
}

