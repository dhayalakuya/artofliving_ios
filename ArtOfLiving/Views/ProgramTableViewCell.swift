//
//  ProgramTableViewCell.swift
//  ArtOfLiving
//
//  Created by Dhayanithi on 14/02/18.
//  Copyright © 2018 Kuya. All rights reserved.
//

import UIKit

public typealias ProgramItemSelectionHandler = () -> Void

class ProgramTableViewCell: UITableViewCell {

    var label: UILabel?
    var backgroundImageView: UIImageView?

    var handler : ProgramItemSelectionHandler?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = UITableViewCellSelectionStyle.none
        self.contentView.backgroundColor = UIColor.clear
        
        self.clipsToBounds = false
        
        customInitialization()
    }
    
    func customInitialization() {

        self.backgroundColor = UIColor.clear
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundView?.backgroundColor = UIColor.clear
        
        if backgroundImageView == nil{
            backgroundImageView = UIImageView.init(image: UIImage.init(named: "beginners"))
            backgroundImageView?.frame =  self.bounds
            backgroundImageView?.layer.cornerRadius = 5.0
            backgroundImageView?.clipsToBounds = true
            backgroundImageView?.layer.borderColor = UIColor.white.cgColor
            backgroundImageView?.layer.borderWidth = 0.0
            self.addSubview(backgroundImageView!)
        }
        
        if label == nil{
            label = UILabel.init(frame: CGRect.init(x: 0, y: bounds.size.height - 50, width: bounds.size.width, height: 30))
            label?.textAlignment = .center
            label?.textColor = UIColor.white
            label?.backgroundColor = UIColor.black.withAlphaComponent(0.2)
            label?.numberOfLines = 0
            label?.lineBreakMode = .byWordWrapping
            self.addSubview(label!)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customInitialization()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        backgroundImageView?.frame =  CGRect.init(x: 20, y: 5, width: bounds.size.width - 40, height: bounds.size.height - 10)
        label?.frame =  CGRect.init(x: (backgroundImageView?.frame.origin.x)!, y: bounds.size.height - 50, width: (backgroundImageView?.frame.size.width)!, height: 50)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        customInitialization()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        //        imageLayer!.borderColor  = selected ? UIColor.orange.cgColor : borderColor?.cgColor
        //        cellTitleLabel!.textColor = selected ? UIColor.white : borderColor
    }
}

