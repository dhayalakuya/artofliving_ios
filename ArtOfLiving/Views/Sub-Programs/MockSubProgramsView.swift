//
//  MockSubProgramsView.swift
//  ArtOfLiving
//
//  Created by Dhayanithi on 21/02/18.
//  Copyright © 2018 Kuya. All rights reserved.
//

import UIKit

class MockSubProgramsView: UIView {
    
    @IBOutlet weak var backGroundImageView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    convenience init(){
        self.init(frame: CGRect.zero)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.frame = frame
        intialization()
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)!
        
       loadXIB()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //proceed
    }

    func loadXIB() {
        
        let view = loadFromNib()
        addSubview(view)
        addConstraintsTo(view: view)
    }
    
    func loadFromNib() -> UIView {
        
        let selfType = type(of: self)
        let bundle = Bundle(for: selfType)
        let nibName = String(describing: selfType)
        let nib = UINib(nibName: nibName, bundle: bundle)
        
        return nib.instantiate(withOwner: self, options: nil).first! as! UIView
    }
    
    func addConstraintsTo(view: UIView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            view.topAnchor.constraint(equalTo: topAnchor),
            view.leftAnchor.constraint(equalTo: leftAnchor),
            view.rightAnchor.constraint(equalTo: rightAnchor),
            view.bottomAnchor.constraint(equalTo: bottomAnchor)
            ])
    }
    
    func intialization(){
        
    }
}
