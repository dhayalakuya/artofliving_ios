//
//  SubProgramsCollectionViewCell.swift
//  Test
//
//  Created by Dhayanithi on 04/01/18.
//  Copyright © 2018 Dhayanithi. All rights reserved.
//

import UIKit

class SubProgramsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var moreDetailsButton: UIButton!

    @IBOutlet weak var bgView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        bgView.layer.borderColor = UIColor.white.cgColor
        bgView.layer.borderWidth = 0.5
        
        profileImageView.layer.shadowColor = UIColor.black.cgColor
        profileImageView.layer.shadowOpacity = 1
        profileImageView.layer.shadowOffset = CGSize.zero
        profileImageView.layer.shadowRadius = 10
        profileImageView.layer.shouldRasterize = true
        profileImageView.clipsToBounds = false
    }
    
    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)
//        let circularlayoutAttributes = layoutAttributes as! VisualCollectionViewLayoutAttributes
//        self.layer.anchorPoint = circularlayoutAttributes.anchorPoint
//        self.transform = CGAffineTransform.init(rotationAngle: -circularlayoutAttributes.angle)
//        self.center.y += (circularlayoutAttributes.anchorPoint.y - 0.5) * self.bounds.height
//        bgView?.layer.cornerRadius = 10.0
//        bgView.layer.borderColor = UIColor.init(red: 205/255.0, green: 108/255.0, blue: 32/255.0, alpha: 1.0).cgColor
//        bgView.layer.borderWidth = 2.0
//        bgView.clipsToBounds = true
        
//        profileImageView.layer.borderColor = UIColor.init(red: 205/255.0, green: 108/255.0, blue: 32/255.0, alpha: 1.0).cgColor
//
//        profileImageView.layer.cornerRadius = profileImageView.frame.size.height / 2.0
//        profileImageView.clipsToBounds = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        profileImageView.layer.shadowPath = UIBezierPath(rect: profileImageView.bounds).cgPath
    }
}
