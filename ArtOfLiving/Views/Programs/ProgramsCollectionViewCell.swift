//
//  ProgramsCollectionViewCell.swift
//  Test
//
//  Created by Dhayanithi on 04/01/18.
//  Copyright © 2018 Dhayanithi. All rights reserved.
//

import UIKit

class ProgramsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var nameImageView: UIImageView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)
//        let circularlayoutAttributes = layoutAttributes as! UICollectionViewLayoutAttributes
//        self.layer.anchorPoint = circularlayoutAttributes.anchorPoint
//        self.transform = CGAffineTransform.init(rotationAngle: -circularlayoutAttributes.angle)
//        self.center.y += (circularlayoutAttributes.anchorPoint.y - 0.5) * self.bounds.height
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.layer.cornerRadius = self.bounds.height / 2.0
        self.clipsToBounds = true

//        self.layer.cornerRadius = 5.0
//        self.nameImageView.layer.cornerRadius = self.nameImageView.frame.size.height / 2.0
//        self.nameImageView.clipsToBounds = true
    }
}
