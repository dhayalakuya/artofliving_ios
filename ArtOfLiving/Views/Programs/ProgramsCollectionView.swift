//
//  ProgramsCollectionView.swift
//  Test
//
//  Created by Dhayanithi on 16/01/18.
//  Copyright © 2018 Dhayanithi. All rights reserved.
//

import UIKit

class ProgramsCollectionView: UICollectionView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        var index = 0
        for item in self.visibleCells {
            
            if index != 1 {
                var rect = item.frame
                rect.origin.y = 20
                item.frame = rect
            }
            else{
                var rect = item.frame
                rect.origin.y = 20
                item.frame = rect
            }
            
            index = index + 1
        }
        
        var visibleRect = CGRect()
        
        visibleRect.origin = self.contentOffset
        visibleRect.size = self.bounds.size
        
        let visiblePoint = CGPoint.init(x: visibleRect.midX, y: visibleRect.midY)
        let visibleIndexPaths = self.indexPathForItem(at: visiblePoint)
        
        if let visibleIndexPaths = visibleIndexPaths{
            let item = self.cellForItem(at: visibleIndexPaths)
            var rect = item?.frame
            rect?.origin.y = 0
            item?.frame = rect!
        }

    }

    // pragma : Colleection View Data Source
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CustomCollectionViewCell", for: indexPath) as! ProgramsCollectionViewCell
        
        cell.nameLabel.text = "Name"
        
        cell.backgroundColor = UIColor.white
        cell.backgroundView?.backgroundColor = UIColor.white
        cell.contentView.backgroundColor = UIColor.white
        
        return cell
    }
    
    //    - (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath NS_AVAILABLE_IOS(8_0);
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        if indexPath.row == 3{
//            cell.layer.zPosition = 999
//        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.black
        cell.backgroundView?.backgroundColor = UIColor.black
        cell.contentView.backgroundColor = UIColor.black
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    // pragma : Colleection View Delegates
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView (collectionView: UICollectionView, heightAtindexPath indexPath: IndexPath) -> CGFloat {
        return 1
    }
}
