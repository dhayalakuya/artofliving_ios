//
//  ProgramDescriptionView.swift
//  ArtOfLiving
//
//  Created by Dhayanithi on 16/02/18.
//  Copyright © 2018 Kuya. All rights reserved.
//

import UIKit

class ProgramDescriptionView: UIView {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    convenience init(){
        self.init(frame: CGRect.zero)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.frame = frame
        intialization()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        intialization()
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)!
        
        intialization()
    }
    
    func intialization(){
        
        
        
    }


}
