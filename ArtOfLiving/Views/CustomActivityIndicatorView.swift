	//
//  CustomActivityIndicatorView.swift
//  ArtOfLiving
//
//  Created by Dhayanithi on 02/02/18.
//  Copyright © 2018 Kuya. All rights reserved.
//

import UIKit

class CustomActivityIndicatorView: UIView {

    var loaderView = CustomLoader()
    let duration : Double = 0.5
    let greyView = UIView()
    var loadingStopped : Bool = true
    var imageView = UIImageView()
    var backgroundImageView = UIImageView()

    var navigationFrame : CGRect?{
        didSet{
            greyView.frame = CGRect.init(x: 0, y:navigationFrame?.minY ?? 64, width: self.bounds.size.width, height: self.bounds.size.height)
        }
    }

    convenience init(){
        self.init(frame: CGRect.zero)
    }
    
    deinit {
        self.loadingStopped = true
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.frame = frame
        intialization()
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)!
        
        intialization()
    }
    
    func intialization(){
        
        self.backgroundColor = UIColor.clear
        addLoaderView()
        
        self.isUserInteractionEnabled = false
        navigationFrame = CGRect.zero
    }
    
    func addLoaderView(){
        
        if backgroundImageView.superview == nil{
            backgroundImageView = UIImageView.init(frame: CGRect.init(x: bounds.origin.x - 20, y: bounds.origin.y - 20, width: bounds.width / 2 + 40, height: bounds.width / 2 + 40))
            backgroundImageView.isHidden = true
            backgroundImageView.image = UIImage.init(named: "loader")
            backgroundImageView.isUserInteractionEnabled = false
            backgroundImageView.contentMode = .scaleToFill
            backgroundImageView.clipsToBounds = true
            backgroundImageView.layer.cornerRadius = loaderView.frame.size.width / 2.0
            self.addSubview(backgroundImageView)
        }
        
        if imageView.superview == nil{
            imageView = UIImageView.init(frame: CGRect.init(x: bounds.origin.x, y: bounds.origin.y, width: bounds.width / 4, height: bounds.width / 4))
            imageView.isHidden = true
            imageView.image = UIImage.init(named: "aol_white")
            imageView.isUserInteractionEnabled = false
            imageView.contentMode = .scaleAspectFit
            self.addSubview(imageView)
        }
        
        if greyView.superview == nil{
            greyView.isHidden = true
            greyView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
            greyView.isUserInteractionEnabled = false
            self.addSubview(greyView)
        }
        
        greyView.frame = self.bounds

        if loaderView.superview == nil{
            loaderView = CustomLoader.init(frame: CGRect.init(x: bounds.origin.x, y: bounds.origin.y, width: bounds.width / 3, height: bounds.width / 3))
            loaderView.center = self.center
            loaderView.isHidden = true

            self.addSubview(loaderView)
        }
    }

    func rotateBorder(view: UIView, duration: Double){
        UIView.animate(withDuration: duration, delay: 0.0, options: [.curveLinear], animations: {
            view.transform = view.transform.rotated(by: CGFloat(-Double.pi))
        }) { (finished) in
            if self.loadingStopped == false{
                self.rotateBorder(view: view, duration: duration)
            }
        }
    }
    
    func start(){
        if loaderView.superview == nil{
            self.addSubview(loaderView)
        }
        
        loaderView.isHidden = false
        greyView.isHidden = false
        imageView.isHidden = true
        backgroundImageView.isHidden = true
        loadingStopped = false
        rotateBorder(view: loaderView, duration: duration)
    }
    
    func stop(){
        if loaderView.superview != nil{
            loaderView.removeFromSuperview()
        }
        
        loadingStopped = true
        imageView.isHidden = true
        backgroundImageView.isHidden = true
        greyView.isHidden = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        greyView.frame = CGRect.init(x: 0, y:navigationFrame?.minY ?? 64, width: self.bounds.size.width, height: self.bounds.size.height)
        imageView.frame = CGRect.init(x: loaderView.frame.minX + 20, y: loaderView.frame.minY + 20, width: loaderView.frame.size.width - 40.0, height: loaderView.frame.size.height - 40.0)
        backgroundImageView.frame = CGRect.init(x: loaderView.frame.minX + 10, y: loaderView.frame.minY + 10, width: loaderView.frame.size.width - 20.0, height: loaderView.frame.size.height - 20)
        backgroundImageView.clipsToBounds = true
        backgroundImageView.layer.cornerRadius = backgroundImageView.frame.size.width / 2.0

//        greyView.frame = self.bounds
//        loaderView.center = self.center
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
