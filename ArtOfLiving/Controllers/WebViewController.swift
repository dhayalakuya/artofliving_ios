//
//  WebViewController.swift
//  ArtOfLiving
//
//  Created by Dhayanithi on 17/01/18.
//  Copyright © 2018 Kuya. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: BaseViewController, WKNavigationDelegate {

    var webView: WKWebView!
    var bgImageView: UIImageView!
    var menuItem = Menus.Programs.rawValue
    var titleLabel : UILabel!
    var emptyWebViewLabel : UILabel!
    var loadingView = CustomActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setBasicSettings()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setTitleLabel()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addCustomLoadingView(){
        
        loadingView = CustomActivityIndicatorView.init(frame: self.view.bounds)
        loadingView.center = self.view.center
        loadingView.navigationFrame = CGRect.init(x: 0, y: (navigationController != nil) ? (navigationController?.navigationBar.frame.maxY)! : 64, width: self.view.bounds.size.width, height: self.view.bounds.size.height)
        view.addSubview(loadingView)
    }

    func setBasicSettings(){
        
        bgImageView = UIImageView.init(frame: view.bounds)
        bgImageView.image = UIImage.init(named: "launch_image")
        view.addSubview(bgImageView)
        
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: CGRect.init(x: 0, y: UIApplication.shared.statusBarFrame.size.height + (navigationController?.navigationBar.frame.size.height)!, width: view.bounds.width, height: view.bounds.height), configuration: webConfiguration)
        webView.navigationDelegate = self
        webView.sizeToFit()
        view.addSubview(webView)

        addCustomLoadingView()
        
        emptyWebViewLabel = UILabel.init(frame: CGRect.init(x: 0.0, y:  100.0, width: webView.bounds.size.width, height: 30.0))
        emptyWebViewLabel.text = "Coming Soon!"
        emptyWebViewLabel.font = UIFont.systemFont(ofSize: 20.0, weight: UIFont.Weight.medium)
        webView.addSubview(emptyWebViewLabel)
        emptyWebViewLabel.textAlignment = .center
        emptyWebViewLabel.isHidden = true
    }
    
    func setTitleLabel(){
        
        let titleLabelHeight : CGFloat = (navigationController != nil) ? navigationController!.navigationBar.frame.height : CGFloat(44.0)
        let titleLabelWidth : CGFloat = 200.0
        let titleLabelY : CGFloat = (navigationController != nil) ? navigationController!.navigationBar.frame.minY : CGFloat(30.0)

        titleLabel = UILabel.init(frame: CGRect.init(x: clickedPoint.x, y: clickedPoint.y, width: titleLabelWidth, height: titleLabelHeight))
        titleLabel.text = screenName
        titleLabel.backgroundColor = UIColor.clear
        titleLabel.textColor = UIColor.orange
        titleLabel.font = UIFont.boldSystemFont(ofSize: getTitleTextFontSize())
        titleLabel.textAlignment = NSTextAlignment.center
        self.view.addSubview(titleLabel)
        
        UIView.animate(withDuration: 1.0, animations: {
            self.titleLabel.frame = CGRect.init(x: (self.view.frame.size.width - titleLabelWidth) / 2, y: 0, width: titleLabelWidth, height: titleLabelHeight)
        }) { (finished) in
            self.titleLabel.textColor = UIColor.white

            UIView.animate(withDuration: 0.25, animations: {
                self.titleLabel.frame = CGRect.init(x: (self.view.frame.size.width - titleLabelWidth) / 2, y: titleLabelY, width: titleLabelWidth, height: titleLabelHeight)
            }) { (finished) in
                //proceed
            }

//            UIView.animate(withDuration: 5.25, delay: 0.0, usingSpringWithDamping: 0.2, initialSpringVelocity: 8.0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
//                self.titleLabel.frame = CGRect.init(x: (self.view.frame.size.width - titleLabelWidth) / 2, y: titleLabelY, width: titleLabelWidth, height: titleLabelHeight)
//
//            }, completion: { (finished) in
//                //proceed
//            })
        }
    }
    
    func loadWebView() {
        loadWebView(webView: webView, urlString: Menus.getWebURLForIndex(index: menuItem))
    }

    @objc override func backButtonTapped() {
        
        self.audioPlayer.play()
        
        if webView.canGoBack == true{
            webView.goBack()
        }
        else{
            super.backButtonTapped()
        }
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        self.audioPlayer.play()
        
        loadingView.start()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        loadingView.stop()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension WebViewController{
    
    func loadWebView(webView: WKWebView, urlString: String) {
        
        if urlString.isEmpty == false{
            let urlRequest = URLRequest.init(url: URL.init(string: urlString)!)
            webView.load(urlRequest)
        }
        else{
            emptyWebViewLabel.isHidden = false
            view.backgroundColor = UIColor.white
        }
        
    }
}
