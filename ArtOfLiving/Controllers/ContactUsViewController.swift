//
//  ContactUsViewController.swift
//  ArtOfLiving
//
//  Created by Dhayanithi on 16/01/18.
//  Copyright © 2018 Kuya. All rights reserved.
//

import UIKit
import WebKit

class ContactUsViewController: WebViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        menuItem = Menus.ContactUs.rawValue
        loadWebView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
