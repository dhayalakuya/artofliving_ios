//
//  BaseViewController.swift
//  ArtOfLiving
//
//  Created by Dhayanithi on 17/01/18.
//  Copyright © 2018 Kuya. All rights reserved.
//

import UIKit
import CoreMotion
import AVFoundation

class BaseViewController: UIViewController, UIGestureRecognizerDelegate {

    var screenName : String = "Jai Gurudev"
    var activityIndicator = UIActivityIndicatorView()
    var greyView = UIView()
    let coreMotionManager = CMMotionManager()
    var audioPlayer = AVAudioPlayer()
    var clickedPoint : CGPoint = CGPoint.zero
    var clickedFrame : CGRect = CGRect.zero
    var clickedAnimatingFrame : CGRect = CGRect.zero
    var maskorigin = CGPoint.zero
    var didAddMask = false
    var label = UILabel()
    var labVisible = false
    var interactionInProgress = false
    var shouldCompleteTransition = false

    var networkManager : NetworkManager = NetworkManager()
    let customNavigationController = CustomNavigationController()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        navigationBarSettings()
        
        addActivityIndicatore()
        prepareAudio()
        prepareGestureRecognizer()
        
//        label = UILabel.init(frame: CGRect.init(x: 100, y: 50, width: 200, height: 20))
//        label.text = "sdsd asd asd asd sad as"
//        label.backgroundColor = UIColor.clear
//        view.addSubview(label)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        view.bringSubview(toFront: label)
        UIView.animate(withDuration: 10) {
//            self.label.mask!.frame.origin = self.maskorigin
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        setActivityIndicator()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
//        if didAddMask {return}
//        didAddMask = true
//
//        let mask = ImageMask()
//        mask.isOpaque = false
//
//        if let titleView = navigationItem.titleView{
//            let w = titleView.bounds.width
//            let h = titleView.bounds.height
//            mask.frame = CGRect(x: -w, y: 0, width: w*2, height: h*2)
//            titleView.mask = mask
//            maskorigin = mask.frame.origin
//        }
        
        
        //            UIView.animate(withDuration: 10) {
        //                self.label.mask!.frame.origin = self.maskorigin
        //            }
    }
    
    func navigationBarSettings(){
        
        transparentNavigationBar()
        if let navigationController = navigationController{
            if navigationController.viewControllers.count > 1 {
                
//                let backButton = UIBarButtonItem.init(image: UIImage.init(named: "back"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonTapped(sender:)))
                
                let backButton = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: 50, height: 50))
                backButton.imageEdgeInsets = UIEdgeInsets.init(top: 0, left: -20, bottom: 0, right: 10)
                backButton.addTarget(self, action: #selector(backButtonTapped), for: UIControlEvents.touchUpInside)
                backButton.imageView?.contentMode = .scaleAspectFit
                let backBarButton = UIBarButtonItem.init(customView: backButton)
                
                backButton.setImage(UIImage.init(named: "back"), for: UIControlState.normal)
                navigationItem.leftBarButtonItems = [backBarButton]
            }
            
            navigationController.navigationBar.tintColor = UIColor.white
            navigationController.navigationBar.barTintColor = UIColor.white
            navigationController.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
            
            let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(navigationBarTapped))
            tapGesture.numberOfTapsRequired = 1
            tapGesture.delegate = self
            navigationController.navigationBar.addGestureRecognizer(tapGesture)
            
            navigationController.interactivePopGestureRecognizer?.isEnabled = true
            navigationController.interactivePopGestureRecognizer?.delegate = self
            
//            let swipeLeft = UISwipeGestureRecognizer()
//            swipeLeft.addTarget(self, action: #selector(backButtonTapped))
//            swipeLeft.direction = .left
//            self.view.addGestureRecognizer(swipeLeft)
        }
        
        //set navigation title
        if self is WebViewController || self is ProgramsViewController{
//            navigationItem.title = screenName
        }
        else{
            let imageView = UIImageView.init(image: UIImage.init(named: "aol_white"))
            imageView.frame = CGRect.init(x: 0, y: 0, width: 100, height: 42)
            imageView.contentMode = .scaleAspectFit
            navigationItem.titleView = imageView
        }
    }

    func transparentNavigationBar(){
        
        navigationController?.navigationBar.backgroundColor = UIColor.clear
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.isTranslucent = true
    }
    
    @objc func backButtonTapped() {
        navigationController?.popViewController(animated: true)
    }
    
    @objc func navigationBarTapped() {
        
//        labVisible = !labVisible
//        UIView.animate(withDuration: 12.5) {
//            self.navigationItem.titleView!.mask!.frame.origin = self.labVisible ?
//                .zero : self.maskorigin
//        }
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension BaseViewController{
    
    func addActivityIndicatore(){
        activityIndicator.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        activityIndicator.color = UIColor.init(red: 226.0/255.0, green: 111.0/255.0, blue: 35.0/255.0, alpha: 1.0)
        activityIndicator.transform = CGAffineTransform.init(scaleX: 2.0, y: 2.0)
        self.view.addSubview(activityIndicator)
        
        //greyview
        greyView = UIView.init(frame: CGRect.init(x: 0, y: (navigationController != nil) ? (navigationController?.navigationBar.frame.maxY)! : 64, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
        greyView.isHidden = true
        greyView.backgroundColor = UIColor.init(white: 0, alpha: 0.4)
        greyView.isUserInteractionEnabled = false
        self.view.addSubview(greyView)
        
        activityIndicator.hidesWhenStopped = true
    }
    
    func setActivityIndicator(){
        var frame = activityIndicator.frame
        frame.origin.x = (self.view.frame.size.width - frame.size.width) / 2.0
        frame.origin.y = (self.view.frame.size.height - frame.size.height) / 2.0
        activityIndicator.frame = frame
        self.view.bringSubview(toFront: greyView)
        self.view.bringSubview(toFront: activityIndicator)
    }
    
    func startActivity(){
        greyView.isHidden = false
        activityIndicator.startAnimating()
    }
    
    func stopActivity(){
        greyView.isHidden = true
        activityIndicator.stopAnimating()
    }
}

extension BaseViewController{
    
    func prepareAudio(){
        
        let bundlePath = Bundle.main.path(forResource: "beep", ofType: "wav")
        if let bundlePath = bundlePath{
            do{
                self.audioPlayer = try AVAudioPlayer.init(contentsOf: URL.init(string: bundlePath)!)
                self.audioPlayer.prepareToPlay()
            }
            catch{
                //leave
            }
        }
    }
}

extension BaseViewController{
    
    func getTitleTextFontSize() -> CGFloat{
        
        if Utilities.getDeviceName() == DeviceNames.iPhone5{
            return 15.0
        }
        else if Utilities.getDeviceName() == DeviceNames.iPhone6To8{
            return 16.0
        }
        else if Utilities.getDeviceName() == DeviceNames.iPhone6STo8S{
            return 18.0
        }
        else if Utilities.getDeviceName() == DeviceNames.iPhoneX{
            return 18.0
        }
        
        return 15.0
    }
    
    func getMenuCellTitleFontSize() -> CGFloat{
        
        if Utilities.getDeviceName() == DeviceNames.iPhone5{
            return 15.0
        }
        else if Utilities.getDeviceName() == DeviceNames.iPhone6To8{
            return 17.0
        }
        else if Utilities.getDeviceName() == DeviceNames.iPhone6STo8S{
            return 18.0
        }
        else if Utilities.getDeviceName() == DeviceNames.iPhoneX{
            return 18.0
        }
        
        return 15.0

    }
    
}

extension BaseViewController{
    
    private func prepareGestureRecognizer() {
        let gesture = UIScreenEdgePanGestureRecognizer(target: self,
                                                       action: #selector(handleGesture(_:)))
        gesture.delegate = self
        gesture.edges = .left
        view.addGestureRecognizer(gesture)
    }

    @objc func handleGesture(_ gestureRecognizer: UIScreenEdgePanGestureRecognizer) {

        let translation = gestureRecognizer.translation(in: gestureRecognizer.view!.superview!)
        var progress = (translation.x / 200)
        progress = CGFloat(fminf(fmaxf(Float(progress), 0.0), 1.0))
        
        switch gestureRecognizer.state {
        case .began:
            interactionInProgress = true
            backButtonTapped()
        case .changed:
            shouldCompleteTransition = progress > 0.5
//            update(progress)
            
        // 4
        case .cancelled:
            interactionInProgress = false
//            cancel()
            
        // 5
        case .ended:
            interactionInProgress = false
//            if shouldCompleteTransition {
//                finish()
//            } else {
//                cancel()
//            }
        default:
            break
        }
    }

}

extension UIView{
    
    func getScreenShotImage() -> UIImage?{
        
        UIGraphicsBeginImageContext(self.frame.size)
        self.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return image
    }
    
}

class ImageMask : UIView{
    
    override func draw(_ rect: CGRect) {
        let radius = CGRect.init(x: 0, y: 0, width: rect.size.width / 2.0, height: rect.size.height / 2.0)
        UIColor.black.setFill()
        UIGraphicsGetCurrentContext()?.fill(radius)
    }
    
}

extension BaseViewController{
    
    func showAlertWithTitle(title: String, message: String, tintColor: UIColor, showCancelButton: Bool, okButtonTitle: String?, cancelButtonTitle: String? = NSLocalizedString("Cancel", comment: ""), tapBlock: (()->(Void))?) {
        let cancelButtonTitle: String? = showCancelButton ? cancelButtonTitle
            : okButtonTitle
        let otherButtonTitle: String? = showCancelButton ? okButtonTitle : nil
        
         let alertController = UIAlertController.init(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        
        if let otherButtonTitle = otherButtonTitle {
            alertController.addAction(UIAlertAction.init(title: cancelButtonTitle, style: .cancel, handler: { (alertAction) in
                //proceed
            }))

            alertController.addAction(UIAlertAction.init(title: otherButtonTitle, style: .default, handler: { (alertAction) in
                tapBlock?()
            }))
        } else {
            alertController.addAction(UIAlertAction.init(title: cancelButtonTitle, style: .cancel, handler: { (alertAction) in
                tapBlock?()
            }))
        }
        
        alertController.view.tintColor = tintColor
        present(alertController, animated: true, completion: nil)
    }
    
}
