//
//  ProgramsViewController.swift
//  ArtOfLiving
//
//  Created by Dhayanithi on 16/01/18.
//  Copyright © 2018 Kuya. All rights reserved.
//

import UIKit

var ClickedAnimatingFrame : CGRect = CGRect.zero

let Section_Headers = ["Beginners program", "Advanced programs", "Kids and Teens program", "Mahasivaratri", "Sanyam", "Tera may birthday special programs", "Gurupuja", "Chaitra navaratri"]

class ProgramsViewController: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegateFlowLayout, UIViewControllerTransitioningDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var mockSubProgramsView: MockSubProgramsView!
    
    @IBOutlet weak var programTableView: UITableView!
    @IBOutlet weak var subProgramsCollectionView: UICollectionView!
    
    @IBOutlet weak var grayView: UIView!
    
    var programs = [Program]()
    var subPrograms = [SubProgram]()
    var filteredSubPrograms = [SubProgram]()

    var animator: (AOLLayoutAttributesAnimator, Bool, Int, Int) = (AOLCubeLayoutAnimator() as AOLLayoutAttributesAnimator, false, 1, 1)
    var titleLabel : UILabel!

    let transitionAnimator = ModalTransitionAnimator()
    
    var originalImageRect : CGRect = CGRect.zero
    var selectedIndex : Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        setCollectionViewSettings()
        
//        menuItem = Menus.Programs.rawValue
//        loadWebView()
        
        setTableViewSettings()
        getProgramTypes()
        setCollectionViewSettings()
        addBasicSettings()
        
        navigationController?.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //proceed
        if originalImageRect.size.width == 0{
            setTitleLabel()
        }
        else{
//            animate image view
            animateBackToMockSubProgramView()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        subProgramsCollectionView.reloadData()
        
        if originalImageRect.size.width == 0{
            originalImageRect = mockSubProgramsView.profileImageView.frame
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        //proceed
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
//        adjustCollectionViewLayouts()
        
    }
    
    func animateBackToMockSubProgramView(){
        
        ClickedAnimatingFrame.origin = CGPoint.init(x: -mockSubProgramsView.frame.origin.x
            , y: -mockSubProgramsView.frame.origin.y)
        self.mockSubProgramsView.profileImageView.frame = ClickedAnimatingFrame
        
        UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: UIViewAnimationOptions.curveEaseInOut, animations: {
            
            self.mockSubProgramsView.profileImageView.frame = self.originalImageRect
            self.mockSubProgramsView.layoutIfNeeded()
        }) { (finished) in
            if finished == true {
                self.subProgramsCollectionView.isHidden = false
                self.mockSubProgramsView.isHidden = true
            }
        }
    }
    
    func setCollectionViewSettings(){
        
        subProgramsCollectionView.register(UINib(nibName: "SubProgramsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SubProgramsCollectionViewCell")
                
        let collectionViewFlowLayout = AOLAnimatedCollectionViewLayout()
        collectionViewFlowLayout.scrollDirection = .vertical
        collectionViewFlowLayout.animator = AOLCubeLayoutAnimator() as AOLLayoutAttributesAnimator
        subProgramsCollectionView.collectionViewLayout = collectionViewFlowLayout
        subProgramsCollectionView?.isPagingEnabled = true
        subProgramsCollectionView!.decelerationRate = UIScrollViewDecelerationRateFast;

        subProgramsCollectionView.backgroundColor = UIColor.clear
        
        subProgramsCollectionView.clipsToBounds = true
        
//        let tapGestureRecognizers = UITapGestureRecognizer.init(target: self, action: #selector(moveToDetailVC))
//        tapGestureRecognizers.numberOfTapsRequired = 1
//        subProgramsCollectionView.addGestureRecognizer(tapGestureRecognizers)
        
//        let tapOneGestureRecognizers = UITapGestureRecognizer.init(target: self, action: #selector(moveToDetailVC))
//        tapOneGestureRecognizers.numberOfTapsRequired = 1
//        grayView.addGestureRecognizer(tapOneGestureRecognizers)

    }

    func setTitleLabel(){
        
        let titleLabelHeight : CGFloat = (navigationController != nil) ? navigationController!.navigationBar.frame.height : CGFloat(44.0)
        let titleLabelWidth : CGFloat = 200.0
        let titleLabelY : CGFloat = (navigationController != nil) ? navigationController!.navigationBar.frame.minY : CGFloat(30.0)
        
        titleLabel = UILabel.init(frame: CGRect.init(x: clickedPoint.x, y: clickedPoint.y, width: titleLabelWidth, height: titleLabelHeight))
        titleLabel.text = screenName
        titleLabel.backgroundColor = UIColor.clear
        titleLabel.textColor = UIColor.orange
        titleLabel.font = UIFont.boldSystemFont(ofSize: getTitleTextFontSize())
        titleLabel.textAlignment = NSTextAlignment.center
        self.view.addSubview(titleLabel)
        
        UIView.animate(withDuration: 1.0, animations: {
            self.titleLabel.frame = CGRect.init(x: (self.view.frame.size.width - titleLabelWidth) / 2, y: 0, width: titleLabelWidth, height: titleLabelHeight)
        }) { (finished) in
            self.titleLabel.textColor = UIColor.white
            
            UIView.animate(withDuration: 0.25, animations: {
                self.titleLabel.frame = CGRect.init(x: (self.view.frame.size.width - titleLabelWidth) / 2, y: titleLabelY, width: titleLabelWidth, height: titleLabelHeight)
            }) { (finished) in
                //proceed
            }
            
            //            UIView.animate(withDuration: 5.25, delay: 0.0, usingSpringWithDamping: 0.2, initialSpringVelocity: 8.0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
            //                self.titleLabel.frame = CGRect.init(x: (self.view.frame.size.width - titleLabelWidth) / 2, y: titleLabelY, width: titleLabelWidth, height: titleLabelHeight)
            //
            //            }, completion: { (finished) in
            //                //proceed
            //            })
        }
    }

    @objc func moveToDetailVC(){
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProgramDetailViewController") as? ProgramDetailViewController

        if let vc = vc{
            var rect = mockSubProgramsView.profileImageView.frame
            rect.origin = CGPoint.init(x: rect.origin.x + mockSubProgramsView.frame.origin.x - 28, y: rect.origin.y + mockSubProgramsView.frame.origin.y)

            vc.clickedPoint = CGPoint.init(x: 0, y: self.mockSubProgramsView.convert((mockSubProgramsView.nameLabel.frame), to: self.mockSubProgramsView.superview).origin.y)
            vc.clickedFrame = self.mockSubProgramsView.convert(originalImageRect, to: self.mockSubProgramsView.superview)


            vc.screenName = mockSubProgramsView.nameLabel.text!
//            vc.transitioningDelegate = self
//            vc.modalPresentationStyle = .overCurrentContext
//            present(vc, animated: true, completion: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    override func backButtonTapped() {
        if subProgramsCollectionView.isHidden == false{
            hideSubProgramsCollectionView()
        }
        else{
            super.backButtonTapped()
        }
    }
    
    func hideSubProgramsCollectionView() {
        
        self.subProgramsCollectionView.isHidden = true
        self.mockSubProgramsView.isHidden = false
        
        self.titleLabel.text = "Programs"
        self.titleLabel.alpha = 0.0
        
        UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: UIViewAnimationOptions.curveEaseInOut, animations: {
            
            self.mockSubProgramsView.profileImageView.frame  = self.clickedFrame
            self.mockSubProgramsView.layoutIfNeeded()
            self.titleLabel.alpha = 1.0

            self.mockSubProgramsView.profileImageView.layer.shadowRadius = 0.0
            
            self.mockSubProgramsView.nameLabel.alpha = 0.0
            self.mockSubProgramsView.descriptionLabel.alpha = 0.0
            self.mockSubProgramsView.backGroundImageView.alpha = 0.0
            self.grayView.alpha = 0.0
            self.mockSubProgramsView.layoutIfNeeded()
        }) { (finished) in
            if finished == true {
                self.mockSubProgramsView.isHidden = true
                self.subProgramsCollectionView.isHidden = true
                self.grayView.isHidden = true
            }
        }
    }
    
    func addBasicSettings(){
    
        grayView.isHidden = true
        subProgramsCollectionView.isHidden = true
        self.mockSubProgramsView.isHidden = true
//        let tapGesture = UITapG
    }
    
    // pragma : Colleection View Data Source
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: SubProgramsCollectionViewCell.self), for: indexPath) as! SubProgramsCollectionViewCell
        cell.nameLabel.text = filteredSubPrograms[indexPath.item].name
        cell.descriptionLabel.text = "The first step into the Art of Living, the 3-day programme equips participants with practical knowledge and techniques to unlock their deepest potential and bring fullness to life. It is the beginning of a beautiful journey that brings us to the joy and happiness that lies at our very core. "
        
        if indexPath.row % 2 == 1{
            cell.backgroundColor = UIColor.clear
            cell.backgroundView?.backgroundColor = UIColor.clear
            cell.contentView.backgroundColor = UIColor.clear
        }
        else {
            cell.backgroundColor = UIColor.clear
            cell.backgroundView?.backgroundColor = UIColor.clear
            cell.contentView.backgroundColor = UIColor.clear
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filteredSubPrograms.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        guard let animator = animator else { return self.view.bounds.size }
        return CGSize(width: collectionView.bounds.width / CGFloat(animator.2), height: collectionView.bounds.height / CGFloat(animator.3))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.item
        
        let cell = collectionView.cellForItem(at: indexPath) as! SubProgramsCollectionViewCell
        mockSubProgramsView.profileImageView.image = cell.profileImageView.image
        mockSubProgramsView.nameLabel.text = cell.nameLabel.text
        mockSubProgramsView.descriptionLabel.text = cell.descriptionLabel.text
        subProgramsCollectionView.isHidden = true
        mockSubProgramsView.isHidden = false
        
        moveToDetailVC()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func animateMockView(){
        
        
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ProgramsViewController {
    
    func setTableViewSettings(){
        
        programTableView.register(UITableViewCell.self, forCellReuseIdentifier: "ProgramTableViewCell")
        programTableView.delegate = self
        programTableView.dataSource = self
        programTableView.isHidden = false
        programTableView.backgroundColor = UIColor.clear
        programTableView.backgroundView?.backgroundColor = UIColor.clear
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.programs.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.bounds.width, height: 50))
//        view.backgroundColor = UIColor.init(red: 206/255.0, green: 105/255.0, blue: 32/255.0, alpha: 1.0)
        view.backgroundColor = UIColor.clear
        let label = UILabel.init(frame: view.bounds)
        label.text = "I would like to register for"
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 15)
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.white

        view.addSubview(label)
        return view
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let CellIdentifier = "ProgramTableViewCell"
        
        var cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier) as? ProgramTableViewCell
        if (cell == nil) {
            cell = ProgramTableViewCell.init(style: UITableViewCellStyle.default, reuseIdentifier: CellIdentifier)
        }
    
        cell?.handler = {() -> Void in
            
            let rect = self.programTableView.convert((cell?.frame)!, to: self.subProgramsCollectionView.superview)
            self.mockSubProgramsView.frame = rect
            self.mockSubProgramsView.isHidden = false

            //proceed
//            self.subProgramsCollectionView.reloadData()
//            self.subProgramsCollectionView.isHidden = false
//            self.view.sendSubview(toBack: self.programTableView)
//            self.view.bringSubview(toFront: self.subProgramsCollectionView)
//
//            let originalRect = self.subProgramsCollectionView.frame
//
//            self.subProgramsCollectionView.frame = rect
//            self.subProgramsCollectionView.clipsToBounds = false
            
            UIView.animate(withDuration: 5.5, animations: {
                self.grayView.isHidden = false
//                self.subProgramsCollectionView.frame = originalRect
            })
        }
        
        // Configure the cell...
        cell?.contentView.backgroundColor = UIColor.clear
        cell?.label?.text = programs[indexPath.row].name

        return cell ?? UITableViewCell()
    }
    
    // MARK: - Table view delegates
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        filteredSubPrograms = SubProgram.subProgramTypeWithProgramId(programTypeId: Int(programs[indexPath.row].serverId), context: networkManager.persistentService.managedObjectContext)
        
        if filteredSubPrograms.count > 0 {
            
            let cell = tableView.cellForRow(at: indexPath) as! ProgramTableViewCell
            
            clickedFrame = self.programTableView.convert((cell.frame), to: self.mockSubProgramsView)
            self.mockSubProgramsView.isHidden = false
            self.grayView.isHidden = false
            
            self.mockSubProgramsView.backgroundColor = UIColor.clear
            self.mockSubProgramsView.nameLabel.alpha = 0.0
            self.mockSubProgramsView.descriptionLabel.alpha = 0.0
            self.mockSubProgramsView.backGroundImageView.alpha = 0.0
            self.grayView.alpha = 0.0
            
            self.subProgramsCollectionView.reloadData()
            
            clickedFrame = CGRect.init(x: clickedFrame.origin.x + 20, y: clickedFrame.origin.y + 5, width: clickedFrame.size.width - 40, height: clickedFrame.size.height - 10)
            self.mockSubProgramsView.profileImageView.frame = clickedFrame
            mockSubProgramsView.profileImageView.image = cell.backgroundImageView?.image
            mockSubProgramsView.nameLabel.text = filteredSubPrograms[0].name
            mockSubProgramsView.descriptionLabel.text = "The first step into the Art of Living, the 3-day programme equips participants with practical knowledge and techniques to unlock their deepest potential and bring fullness to life. It is the beginning of a beautiful journey that brings us to the joy and happiness that lies at our very core. "
            
            self.titleLabel.text = "Sub programs"
            self.titleLabel.alpha = 0.0
            
            UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: UIViewAnimationOptions.curveEaseInOut, animations: {
                self.mockSubProgramsView.profileImageView.frame  = self.originalImageRect
                
                self.mockSubProgramsView.nameLabel.alpha = 1.0
                self.mockSubProgramsView.descriptionLabel.alpha = 1.0
                self.mockSubProgramsView.backGroundImageView.alpha = 1.0
                self.grayView.alpha = 0.6
                self.titleLabel.alpha = 1.0
                
                self.mockSubProgramsView.profileImageView.layer.shadowColor = UIColor.black.cgColor
                self.mockSubProgramsView.profileImageView.layer.shadowOpacity = 1
                self.mockSubProgramsView.profileImageView.layer.shadowOffset = CGSize.zero
                self.mockSubProgramsView.profileImageView.layer.shadowRadius = 10
                self.mockSubProgramsView.profileImageView.layer.shouldRasterize = true

            }) { (finished) in
                if finished == true {
                    self.mockSubProgramsView.isHidden = true
                    self.subProgramsCollectionView.isHidden = false
                    self.grayView.isHidden = false
                    self.view.bringSubview(toFront: self.subProgramsCollectionView)
                }
            }
        }
    }
}

extension ProgramsViewController{
    
    func getProgramTypes(){
        
        networkManager.getProgramTypes(user: nil, failure: { (error) in
            if let error = error{
                self.showAlertWithTitle(title: NSLocalizedString("Art Of Living", comment: ""), message: error.description, tintColor: UIColor.black, showCancelButton: false, okButtonTitle: "Ok", tapBlock: nil)
            }
        }) { savedObject, JSON in
            
            self.programs = savedObject as! [Program]
            
            self.programTableView.reloadData()
            
            self.networkManager.getSubProgramTypes(user: nil, failure: { (error) in
                if let error = error{
                    self.showAlertWithTitle(title: NSLocalizedString("Art Of Living", comment: ""), message: error.description, tintColor: UIColor.black, showCancelButton: false, okButtonTitle: "Ok", tapBlock: nil)
                }
            }) { savedObject, JSON in
                self.subPrograms = savedObject as! [SubProgram]
                self.filteredSubPrograms = self.subPrograms
            }
        }
    }
}

extension ProgramsViewController{

    func presentProgramDescriptionView(){
        
        let presentProgramView = UIView.init(frame: CGRect.init(x: 20, y: 40, width: self.view.frame.size.width - 40, height: self.view.frame.size.width - 80))
        
        self.view.addSubview(presentProgramView)
    }
}

// MARK: - Navigation methods
extension ProgramsViewController{
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationControllerOperation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        customNavigationController.reverse = operation == .pop
        return customNavigationController
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        transitionAnimator.originFrame = clickedFrame
        
        transitionAnimator.presenting = true
        return transitionAnimator
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transitionAnimator.presenting = false
        return transitionAnimator
    }
}

