//
//  HomeViewController.swift
//  ArtOfLiving
//
//  Created by Dhayanithi on 16/01/18.
//  Copyright © 2018 Kuya. All rights reserved.
//

import UIKit
import CoreGraphics
import CoreMotion

struct LineModel {
    var startPoint: CGPoint
    var endPoint: CGPoint
    var lineColor: UIColor
    func drawLine() -> CAShapeLayer {
        //create the path
        let linePath = UIBezierPath()
        linePath.move(to: startPoint)
        linePath.addLine(to: endPoint)
        //create the line as a CAShapeLayer
        let lineLayer = CAShapeLayer()
        lineLayer.path = linePath.cgPath
        lineLayer.lineWidth = 3
        lineLayer.strokeColor = lineColor.cgColor
        return lineLayer
    }
}

enum Menus : Int{
    case Programs = 0
    case MyJourney
    case Accommodation
    case Access
    case ePay
    case Map
    case Seva
    case MeetGuruji
    case Panchakarma
    case Shuttle
    case Donate

    static var selectedMenu = Menus.Map

    subscript(index: Int) -> Menus{
        switch index {
        case Menus.MyJourney.rawValue:
            return Menus.MyJourney
        case Menus.Programs.rawValue:
            return Menus.Programs
        case Menus.Accommodation.rawValue:
            return Menus.Accommodation
        case Menus.Access.rawValue:
            return Menus.Access
        case Menus.ePay.rawValue:
            return Menus.ePay
        case Menus.Map.rawValue:
            return Menus.Map
        case Menus.Seva.rawValue:
            return Menus.Seva
        case Menus.MeetGuruji.rawValue:
            return Menus.MeetGuruji
        case Menus.Panchakarma.rawValue:
            return Menus.Panchakarma
        case Menus.Shuttle.rawValue:
            return Menus.Shuttle
        case Menus.Donate.rawValue:
            return Menus.Donate

        default:
            return Menus.Programs
        }
    }
    
    static func getWebURLForIndex(index: Int) -> String{
        
        switch index {
        case Menus.MyJourney.rawValue:
            return ""
        case Menus.Programs.rawValue:
            return "https://programs.org.in/ekam/index.php/public/welcome"
        case Menus.Accommodation.rawValue:
            return ""
        case Menus.Access.rawValue:
            return ""
        case Menus.ePay.rawValue:
            return ""
        case Menus.Map.rawValue:
            return ""
        case Menus.Seva.rawValue:
            return ""
        case Menus.MeetGuruji.rawValue:
            return ""
        case Menus.Panchakarma.rawValue:
            return ""
        case Menus.Shuttle.rawValue:
            return ""
        case Menus.Donate.rawValue:
            return "https://programs.org.in/ekam/index.php/public/donatenow"

        default:
            return "https://www.artofliving.org/in-en/about-us"
        }
    }
    
    static func getClassNameForIndex(index: Int) -> String{
        
        switch index {
        case Menus.Programs.rawValue:
            return "ProgramsViewController"
        case Menus.MyJourney.rawValue:
            return "ProgramsViewController"
        case Menus.Accommodation.rawValue:
            return "AboutUsViewController"
        case Menus.Access.rawValue:
            return "AboutUsViewController"
        case Menus.ePay.rawValue:
            return "AboutUsViewController"
        case Menus.Map.rawValue:
            return "AboutUsViewController"
        case Menus.Seva.rawValue:
            return "AboutUsViewController"
        case Menus.MeetGuruji.rawValue:
            return "AboutUsViewController"
        case Menus.Panchakarma.rawValue:
            return "AboutUsViewController"
        case Menus.Shuttle.rawValue:
            return "AboutUsViewController"
        case Menus.Donate.rawValue:
            return "AboutUsViewController"

        default:
            return "ProgramsViewController"
        }
    }
    
    static func getImageNameForIndex(index: Int) -> String{
        
        switch index {
        case Menus.MyJourney.rawValue:
            return "my_journey"
        case Menus.Programs.rawValue:
            return "programs"
        case Menus.Accommodation.rawValue:
            return "accomodation"
        case Menus.Access.rawValue:
            return "access"
        case Menus.ePay.rawValue:
            return "epay"
        case Menus.Map.rawValue:
            return "map"
        case Menus.Seva.rawValue:
            return "seva"
        case Menus.MeetGuruji.rawValue:
            return "meet_guruji"
        case Menus.Panchakarma.rawValue:
            return "panchakarma"
        case Menus.Shuttle.rawValue:
            return "shuttle"
        case Menus.Donate.rawValue:
            return "donate"

        default:
            return "programs"
        }
    }
    
    
    static var menuItems: Array<String>{
        return ["", "Programs", "My Journey", "Accomodation", "Access", "e-Pay", "Map", "Seva", "Meet Guruji", "Panchakarma", "Shuttle", "Donate", ""]
    }
    
//    var menu_Items: Array<String>{
//        return ["About us", "Programs", "Contact us", "FAQ", "Donate"]
//    }
};

let Offset = 40

class HomeViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, UIViewControllerTransitioningDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var backgroundImagesView: UIView!
    @IBOutlet weak var tableView: CircularMenuTableView!
    @IBOutlet weak var ashramImageView: UIImageView!
    
    @IBOutlet weak var gurujiBackgroundImageView: UIImageView!
    @IBOutlet weak var tableViewYConstraint: NSLayoutConstraint!
    @IBOutlet weak var backgroundImageView: UIImageView!
    var isEnableInfiniteScrolling = false
    var contentAlignment = CircularMenuTableViewContentAlignment.right
    
    var imageThemeIndex : Int = 0
    var showGurujiImage : Bool = false
    
    var timer : Timer?

    var lineModal = LineModel(startPoint: CGPoint.zero, endPoint: CGPoint.init(x: 500, y: 500), lineColor: UIColor.black)
    var loadingView = CustomActivityIndicatorView()
    
    var radius: CGFloat = 0.0
    var contentOffsetX : CGFloat = 0.0
    var titleLabel = UILabel()
    var selectedIndex : Int = 5

    var originalOffsetArray = [CGFloat]()
    var currentOffsetDictionary = Dictionary<CGFloat, CGFloat>()
    
    var emitter = CAEmitterLayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tableViewSettings()
        backgroundImageViewSettings()
                
        self.view.addSubview(titleLabel)
        
        navigationController?.delegate = self

        setMotionManager()
        
        self.coreMotionManager.startDeviceMotionUpdates()
        timer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true, block: { (timer) in
            //proceed
            if let deviceMotion = self.coreMotionManager.deviceMotion{

                let accel = deviceMotion.attitude
                print("accel x \(accel.pitch)")

//                print("accel y \(accel.y)")
//
//                print("accel z \(accel.z)")

//                if accel.y >= 2.0 {
//                    print(".......Thrust...... \(accel.y)")
//                }
//
//                if accel.x <= -1.0 || accel.x >= 1.0 {
//                    print(".......Perry...... \(accel.x)")
//                }

//                if accel.z <= -1.0 || accel.z >= 1.0 {
//                    print(".......Perry...... \(accel.z)")
//                }

//                let gyro = deviceMotion.rotationRate
//                print("gyro x \(gyro.x)")
//
//                print("gyro y \(gyro.y)")
//
//                print("gyro z \(gyro.z)")

            }
        })
        
//        setCustomLoadingView()
        
//        addCircleAnimation()
        
//        loadingView = CustomActivityIndicatorView.init(frame: self.view.bounds)
//        loadingView.center = self.view.center
//        view.addSubview(loadingView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        screenName = "Jai Gurudev"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        setTableViewContentOffset()
        setColorToSelectedRow()
        
        animateComet()
        view.layer.addSublayer(emitter)
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        //proceed
        emitter.removeFromSuperlayer()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Mark - Settings
    func backgroundImageViewSettings(){
        
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(setTableViewContentOffset))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.delegate = self
        tableView.addGestureRecognizer(tapGesture)
        
        if Utilities.getDeviceName() == DeviceNames.iPhoneX{
            backgroundImageView.image = UIImage.init(named: "background_image_1")
        }
        else{
            backgroundImageView.image = UIImage.init(named: "background_image")
        }
    }
    
    func setBackgroundImageView(){
        
        if showGurujiImage{

            if Utilities.getDeviceName() == DeviceNames.iPhoneX{
                gurujiBackgroundImageView.image = UIImage.init(named: "guruji_1")
            }
            else{
                gurujiBackgroundImageView.image = UIImage.init(named: "guruji")
            }
            
            UIView.animate(withDuration: 2.5, animations: {
                self.gurujiBackgroundImageView.alpha = 1
                self.ashramImageView.alpha = 0
            }, completion: { (finished) in
                //proceed
            })

            self.backgroundImagesView.bringSubview(toFront: gurujiBackgroundImageView)
            showGurujiImage = false
        }
        else{
            if Utilities.getDeviceName() == DeviceNames.iPhoneX{
                ashramImageView.image = UIImage.init(named: "ashram_\(imageThemeIndex)\(imageThemeIndex)")
            }
            else{
                ashramImageView.image = UIImage.init(named: "ashram_\(imageThemeIndex)")
            }

            showGurujiImage = true
            
            if imageThemeIndex == 1{
                imageThemeIndex = 0
            }
            else{
                imageThemeIndex = imageThemeIndex + 1
            }
            
            UIView.animate(withDuration: 2.5, animations: {
                self.gurujiBackgroundImageView.alpha = 0
                self.ashramImageView.alpha = 1
            }, completion: { (finished) in
                //proceed
            })
            
            self.backgroundImagesView.bringSubview(toFront: ashramImageView)
        }
    }
    
    func tableViewSettings(){
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "CircularMenuTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.radius = radius
        tableView.enableInfiniteScrolling = false
        tableView.contentAlignment = contentAlignment
        
        weak var waekTableView = self.tableView
        waekTableView?.reloadCompletionHandler = {() -> Void in
            waekTableView?.scrollFirstCellToCenter()
        }
        
        waekTableView?.layoutCompletionHandler = {() -> Void in

            for cell in (waekTableView?.visibleCells)! {
                if cell is CircularMenuTableViewCell{
                    let newCell =  cell as! CircularMenuTableViewCell
                    newCell.cellTitleLabel?.textColor = UIColor.white
                    newCell.setBorderColor(color: UIColor.orange)
                }
            }
            
            let selectedCell = waekTableView?.cellAtindex(index: self.selectedIndex)
            selectedCell?.cellTitleLabel?.textColor = UIColor.orange
            selectedCell?.setBorderColor(color: UIColor.white)
        }
        
        tableView.reloadData()
    }
    
    //Mark - Settings
    override func viewWillLayoutSubviews() {
        setTableViewConstraints()
    }
    
    func getTransformSize() -> CGFloat{
        
        if Utilities.getDeviceName() == DeviceNames.iPhone5{
            return 1.2
        }
        else if Utilities.getDeviceName() == DeviceNames.iPhone6To8{
            return 1.2
        }
        else if Utilities.getDeviceName() == DeviceNames.iPhone6STo8S{
            return 1.2
        }
        else if Utilities.getDeviceName() == DeviceNames.iPhoneX{
            return 1.2
        }
        
        return 1.2
    }
    
    func getTranslationSize() -> CGFloat{
        
        if Utilities.getDeviceName() == DeviceNames.iPhone5{
            return -10.0
        }
        else if Utilities.getDeviceName() == DeviceNames.iPhone6To8{
            return 20.0
        }
        else if Utilities.getDeviceName() == DeviceNames.iPhone6STo8S{
            return 50.0
        }
        else if Utilities.getDeviceName() == DeviceNames.iPhoneX{
            return 1.2
        }
        
        return 1.2
    }

    func getCellHeight() -> CGFloat{
        
        if Utilities.getDeviceName() == DeviceNames.iPhone5{
            return 45.0
        }
        else if Utilities.getDeviceName() == DeviceNames.iPhone6To8{
            return 50.0
        }
        else if Utilities.getDeviceName() == DeviceNames.iPhone6STo8S{
            return 60.0
        }
        else if Utilities.getDeviceName() == DeviceNames.iPhoneX{
            return 60.0
        }
        
        return 70.0
    }

    @objc func setTableViewContentOffset() {
        
        var contentOffset = CGPoint.zero
        if Utilities.getDeviceName() == DeviceNames.iPhone5{
            contentOffset = CGPoint.init(x: 0, y: 15)
        }
        else if Utilities.getDeviceName() == DeviceNames.iPhone6To8{
            contentOffset = CGPoint.init(x: 0, y: 0)
        }
        else if Utilities.getDeviceName() == DeviceNames.iPhone6STo8S{
            contentOffset = CGPoint.init(x: 0, y: 46)
        }
        else if Utilities.getDeviceName() == DeviceNames.iPhoneX{
            contentOffset = CGPoint.init(x: 0, y: 44)
        }
        
        if tableView.contentOffset.y != contentOffset.y{
            tableView.setContentOffset(contentOffset, animated: true)
            self.audioPlayer.play()
        }
        
        setBackgroundImageView()
    }
    
    @objc override func navigationBarTapped() {
        super.navigationBarTapped()
        setTableViewContentOffset()
    }
    
    @IBAction func logoutButtonClicked(_ sender: Any) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func setColorToSelectedRow(){
        
        for cell in (tableView.visibleCells) {
            if cell is CircularMenuTableViewCell{
                let newCell =  cell as! CircularMenuTableViewCell
                newCell.cellTitleLabel?.textColor = UIColor.white
                newCell.setBorderColor(color: UIColor.orange)
            }
        }
        
        let selectedCell = tableView.cellAtindex(index: self.selectedIndex)
        selectedCell?.cellTitleLabel?.textColor = UIColor.orange
        selectedCell?.setBorderColor(color: UIColor.white)
    }

    func setTableViewConstraints() {
        
        if Utilities.getDeviceName() == DeviceNames.iPhone5{
            tableViewYConstraint.constant = -30.0
        }
        else if Utilities.getDeviceName() == DeviceNames.iPhone6To8{
            tableViewYConstraint.constant = -30.0
        }
        else if Utilities.getDeviceName() == DeviceNames.iPhone6STo8S{
            tableViewYConstraint.constant = -10.0
        }
        else if Utilities.getDeviceName() == DeviceNames.iPhoneX{
            tableViewYConstraint.constant = 0
        }
    }
}

// MARK: - Gesture metbods
extension HomeViewController{
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        clickedPoint = CGPoint.init(x: touch.location(in: self.view).x - 30, y: touch.location(in: self.view).y - 30)
        
        if touch.view?.superview is UITableViewCell {
            return false
        }
        
        return true
    }
}

// MARK: - Table view data sources
extension HomeViewController{

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return getCellHeight()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Menus.menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let CellIdentifier = "CircularMenuTableViewCell"
        
        var cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier) as? CircularMenuTableViewCell
        if (cell == nil) {
            cell = CircularMenuTableViewCell.init(style: UITableViewCellStyle.default, reuseIdentifier: CellIdentifier)
        }
        
        // Configure the cell...
        cell?.setImage(image: UIImage.init(named: Menus.getImageNameForIndex(index: indexPath.row))!)
        cell?.setCellTitle(title: Menus.menuItems[indexPath.row])
        return cell ?? UITableViewCell()
    }
    
    // MARK: - Table view delegates
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //move to respective page
        
            if indexPath.row >= Menus.Programs.rawValue && indexPath.row <= Menus.Donate.rawValue{
                
                selectedIndex = indexPath.row
                
                Menus.selectedMenu = Menus(rawValue: indexPath.row)!
                
                self.audioPlayer.play()
                
                let cell = self.tableView.cellAtindex(index: indexPath.row)
                
                if let cell = cell{
                    screenName = (cell.cellTitleLabel?.text)!
                    
                    if let point = cell.cellTitleLabel?.convert((cell.cellTitleLabel?.frame)!, to: self.view)
                    {
                        clickedPoint = CGPoint.init(x: point.origin.x - 95, y: point.origin.y - 30)
                    }
                    
                    self.setTitleLabel()
                    screenName = (cell.cellTitleLabel?.text)!
                    
//                    UIView.animate(withDuration: Navigation_Transition, animations: {
//                        //                cell.cellTitleLabel.frame = CGRect.init(x: (self.view.frame.size.width - (cell.cellTitleLabel?.bounds.width)!) / 2, y: 0, width: (cell.cellTitleLabel?.bounds.width)!, height: (cell.cellTitleLabel?.bounds.height)!)
//                        //                self.setTitleLabel()
//                    }) { (finished) in
//
//                        //                cell.isHidden = true
//                        //                if indexPath.row >= Menus.AboutUs.rawValue && indexPath.row <= Menus.Donate.rawValue{
//                        //                    let viewController = self.storyboard?.instantiateViewController(withIdentifier: Menus.getClassNameForIndex(index: indexPath.row))
//                        //                    let baseVC = viewController as? BaseViewController
//                        //
//                        //                    if let baseVC = baseVC{
//                        //                        baseVC.sclickedPoint = self.clickedPoint
//                        //                        baseVC.screenName = Menus.menuItems[indexPath.row]
//                        //                        self.navigationController?.pushViewController(baseVC, animated: true)
//                        //                    }
//                        //
//                        //                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//                        //                        cell.transform = CGAffineTransform.identity;
//                        //                        cell.frame = originalFrame!
//                        //                        cell.isHidden = false
//                        //                    }
//                        //                }
//                    }

                let viewController = self.storyboard?.instantiateViewController(withIdentifier: Menus.getClassNameForIndex(index: indexPath.row))
                let baseVC = viewController as? BaseViewController
                baseVC?.clickedPoint = self.clickedPoint
                
                if let baseVC = baseVC{
                    baseVC.screenName = Menus.menuItems[indexPath.row]
//                    baseVC.menuItem = selectedIndex
                    self.navigationController?.pushViewController(baseVC, animated: true)
                }
            }
        }
    }
    
    func setTitleLabel(){
        
        let titleLabelHeight : CGFloat = (navigationController != nil) ? navigationController!.navigationBar.frame.height : CGFloat(44.0)
        let titleLabelWidth : CGFloat = 120.0
        let titleLabelY : CGFloat = (navigationController != nil) ? navigationController!.navigationBar.frame.minY : CGFloat(30.0)
        
        titleLabel = UILabel.init(frame: CGRect.init(x: clickedPoint.x, y: clickedPoint.y, width: titleLabelWidth, height: titleLabelHeight))
        titleLabel.text = screenName
        titleLabel.backgroundColor = UIColor.clear
        titleLabel.textColor = UIColor.orange
        titleLabel.font = UIFont.boldSystemFont(ofSize: getMenuCellTitleFontSize())
        titleLabel.textAlignment = NSTextAlignment.center
        
        self.view.addSubview(titleLabel)
        
        UIView.animate(withDuration:1.0, animations: {
            self.titleLabel.frame = CGRect.init(x: (self.view.frame.size.width - titleLabelWidth) / 2, y: 0, width: titleLabelWidth, height: titleLabelHeight)
        }) { (finished) in
            self.titleLabel.textColor = UIColor.white
            
            UIView.animate(withDuration: 0.25, animations: {
                self.titleLabel.frame = CGRect.init(x: (self.view.frame.size.width - titleLabelWidth) / 2, y: titleLabelY, width: titleLabelWidth, height: titleLabelHeight)
            }) { (finished) in
                self.titleLabel.removeFromSuperview()
            }
            
//                        UIView.animate(withDuration: 5.25, delay: 0.0, usingSpringWithDamping: 0.2, initialSpringVelocity: 8.0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
//                            self.titleLabel.frame = CGRect.init(x: (self.view.frame.size.width - titleLabelWidth) / 2, y: titleLabelY, width: titleLabelWidth, height: titleLabelHeight)
//
//                        }, completion: { (finished) in
//                            //proceed
//                        })
        }
    }

    
    //            let originalFrame = cell.frame
    //
    //            let transform = CGAffineTransform.init(scaleX: -2, y: -2)
    //            let translation = CGAffineTransform.init(translationX: 100, y: 0)
    //
    //            var frame = originalFrame
    //            frame.origin.x = frame.origin.x - 50
    ////            cell.frame = frame
    //
    //
    //
    //            cell.transform = CGAffineTransform.init(translationX: 250, y: 0)
    //            UIView.animate(withDuration: 1.0,
    //                           delay: 0,
    //                           usingSpringWithDamping: 1.2,
    //                           initialSpringVelocity: 0.0,
    //                           options: .allowUserInteraction,
    //                           animations: { [weak self] in
    //                            cell.transform = CGAffineTransform.init(scaleX: 1.2, y: 1.2)
    //                },
    //                           completion: {finished in
    ////                            cell.transform = .identity
    //                            }
    //                )
    //
    //            UIView.animate(withDuration: 0.25,
    //                           delay: 0,
    //                           usingSpringWithDamping: 0.2,
    //                           initialSpringVelocity: 6.0,
    //                           options: .allowUserInteraction,
    //                           animations: { [weak self] in
    //                            if indexPath.row >= Menus.AboutUs.rawValue && indexPath.row <= Menus.Donate.rawValue{
    //                                let viewController = self?.storyboard?.instantiateViewController(withIdentifier: Menus.getClassNameForIndex(index: indexPath.row))
    //                                let baseVC = viewController as? BaseViewController
    //
    //                                if let baseVC = baseVC{
    //                                    baseVC.screenName = Menus.menuItems[indexPath.row]
    //                                    self?.navigationController?.pushViewController(baseVC, animated: true)
    //                                }
    //
    //                                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
    //                                    cell.transform = CGAffineTransform.identity;
    //                                }
    //                            }
    //                },
    //                           completion: nil)
    //
    //        }
    
    //        let transform = CGAffineTransform.init(scaleX: getTransformSize(), y: getTransformSize())
    //        let translation = CGAffineTransform.init(translationX: 50, y: 0)
    //        cell?.transform = translation;
    
    //        UIView.animate(withDuration: 0.5, animations: {
    ////            cell?.transform = CGAffineTransform.identity.scaledBy(x: self.getTransformSize(), y: self.getTransformSize());
    //        }) { (finished) in
    //
    //            UIView.animate(withDuration: 0.1, animations: {
    ////                cell?.frame = CGRect.init(x: self.view.frame.size.width, y: 0, width: self.view.bounds.size.width, height: (cell?.frame.size.height)!)
    //            }) { (finished) in
    //                cell?.isHidden = true
    //                if indexPath.row >= Menus.AboutUs.rawValue && indexPath.row <= Menus.Donate.rawValue{
    //                    let viewController = self.storyboard?.instantiateViewController(withIdentifier: Menus.getClassNameForIndex(index: indexPath.row))
    //                    let baseVC = viewController as? BaseViewController
    //
    //                    if let baseVC = baseVC{
    //                        baseVC.clickedPoint = self.clickedPoint
    //                        baseVC.screenName = Menus.menuItems[indexPath.row]
    //                        self.navigationController?.pushViewController(baseVC, animated: true)
    //                    }
    //
    //                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
    //                        cell?.transform = CGAffineTransform.identity;
    //                        cell?.frame = originalFrame!
    //                        cell?.isHidden = false
    //                    }
    //                }
    //
    //            }
    
    //        }

}

extension HomeViewController{
    
    func addCircleAnimation(){
        
        let circlePath = UIBezierPath.init(arcCenter: self.view.center, radius: 100, startAngle: 0, endAngle: CGFloat(Double.pi * 2), clockwise: true)
        let circleShape = CAShapeLayer()
        circleShape.path = circlePath.cgPath
        circleShape.fillColor = UIColor.white.cgColor
        circleShape.strokeColor = UIColor.orange.cgColor
        circleShape.lineWidth = 5.0
        
        self.view.layer.addSublayer(circleShape)
        
        let fadeAnimation = CABasicAnimation(keyPath: "position")
        fadeAnimation.duration = 2
        fadeAnimation.fromValue = [0,0]
        fadeAnimation.toValue = [-100, -100]
        circleShape.add(fadeAnimation, forKey: "position")

        let bgAnimation = CABasicAnimation(keyPath: "backgroundColor")
        bgAnimation.fromValue = UIColor.white.cgColor
        bgAnimation.toValue = UIColor.orange.cgColor
        bgAnimation.duration = 2.0
        circleShape.add(bgAnimation, forKey: "backgroundColor")
    }
}

extension HomeViewController{
    
    func animateComet() {
        // create emitter
        
        emitter.emitterShape = kCAEmitterLayerPoint
        emitter.emitterPosition = CGPoint(x: -50, y: view.bounds.height / 3.0)
        emitter.emitterSize = CGSize(width: 300, height: 1.0)
        
        var cells = [CAEmitterCell]()
        for item in 0..<5 {
            // create comet cell
            let cell = CAEmitterCell()
            cell.contents = UIImage(named: "programs")!.cgImage
            //        cell.birthRate = 4
            //        cell.lifetime = 2.0
            //        cell.velocity = 10
            //        cell.emissionRange = 0.25
            //        cell.velocityRange = 0
            //        cell.scale = 0.05
            //        cell.emissionLongitude = calculateAngle()
            
            cell.birthRate = 2.0
            cell.lifetime = 5.0
            cell.lifetimeRange = 0
            cell.velocity = 1
            cell.velocityRange = 500
            //            cell.emissionLongitude = CGFloat(Double.pi * 1)
            //        cell.emissionLatitude = CGFloat(Double.pi * 3)
            cell.emissionRange = 0.25
            cell.spin = 1.0
            cell.spinRange = 0
            cell.color = getNextColor(index: item).cgColor
            //        cell.contents = getNextImage(i: index)
            //        cell.scaleRange = 0.01
            cell.scale = 0.009
            cell.emissionLongitude = CGFloat(item * 30) + calculateAngle()
            cells.append(cell)
        }
        
        //add cell to the emitter
        emitter.emitterCells = cells
    }

    func getNextColor(index: Int) -> UIColor {
        if index / 2 == 0{
            return UIColor.yellow
        }
        else{
            return UIColor.white
        }
    }
    
    func calculateAngle() -> CGFloat {
        let deltaX = 90
        let deltaY = 90
        return atan2(CGFloat(deltaY), CGFloat(deltaX))
    }
    
    func setMotionManager(){

        if coreMotionManager.isGyroAvailable {
            
//            coreMotionManager.gyroUpdateInterval = 0.1
//            coreMotionManager.startGyroUpdates()

            let queue = OperationQueue()
            
            //            coreMotionManager.startGyroUpdates(to: queue, withHandler: { (data, error) in
            //                print("x: \(data?.rotationRate.x)  y: \(data?.rotationRate.y)  z: \(data?.rotationRate.z)")
            //            })

            //            if coreMotionManager.isAccelerometerAvailable {
            //                coreMotionManager.accelerometerUpdateInterval = 1
            //                coreMotionManager.startAccelerometerUpdates(to: .main) {
            //                    [weak self] (data: CMAccelerometerData?, error: Error?) in
            //                    if let acceleration = data?.acceleration {
            //                        let rotation = atan2(acceleration.x, acceleration.y) - Double.pi
            //                        self?.backgroundImageView.transform = CGAffineTransform(rotationAngle: CGFloat(rotation))
            //                    }
            //                }
            //            }

            //            if coreMotionManager.isDeviceMotionAvailable {
            //                coreMotionManager.deviceMotionUpdateInterval = 0.01
            //                coreMotionManager.startDeviceMotionUpdates(to: queue) {
            //                    [weak self] (data: CMDeviceMotion?, error: Error?) in
            //                    if let gravity = data?.gravity {
            //                        let rotation = atan2(gravity.x, gravity.y) - Double.pi
            //                        self?.backgroundImageView.transform = CGAffineTransform(rotationAngle: CGFloat(rotation))
            //                    }
            //                }
            //            }

            if coreMotionManager.isDeviceMotionAvailable {
                
            }
            
            if coreMotionManager.isDeviceMotionAvailable {
                coreMotionManager.deviceMotionUpdateInterval = 0.1
                coreMotionManager.startDeviceMotionUpdates()
//                coreMotionManager.startDeviceMotionUpdates(to: .main) {
//                    [weak self] (data: CMDeviceMotion?, error: Error?) in
//                    let y = data?.rotationRate.x
//                    let x = data?.rotationRate.y
//                    let z = data?.rotationRate.z
//
//                    if y! <= -10.0 || y! >= 10.0 {
//                        print(".......yyyyyyyy...... \(y)")
//                    }
//
//                    if x! <= -10.0 || x! >= 10.0 {
//                        print(".......xxxxxxx...... \(x)")
//                    }
//
//                    if z! <= -10.0 || z! >= 10.0 {
//                        print(".......zzzzzzz...... \(x)")
//                    }
//                }
            }
        }

    }
    
//    func drawLetterANimation(){
//
//        let word = "Test"
//        let path = UIBezierPath()
//        let spacing: CGFloat = 50
//        var i: CGFloat = 0
//        for letter in word.characters {
//            let newPath = self.getPathForLetter(letter: letter)
//            let actualPathRect = CGPathGetBoundingBox(path.CGPath)
//            let transform = CGAffineTransformMakeTranslation((CGRectGetWidth(actualPathRect) + min(i, 1)*spacing), 0)
//            newPath.applyTransform(transform)
//            path.appendPath(newPath)
//            i++
//        }
//    }
//
//        func getPathForLetter(letter: Character) -> UIBezierPath {
//            var path = UIBezierPath()
//            let font = CTFontCreateWithName("HelveticaNeue" as CFString, 64, nil)
//            var unichars = [UniChar]("\(letter)".utf16)
//            var glyphs = CGGlyph.ini [CGGlyph](count: unichars.count, repeatedValue: 0)
//
//            let gotGlyphs = CTFontGetGlyphsForCharacters(font, &unichars, &glyphs, unichars.count)
//            if gotGlyphs {
//                let cgpath = CTFontCreatePathForGlyph(font, glyphs[0], nil)
//                path = UIBezierPath(CGPath: cgpath!)
//            }
//
//            return path
//        }
}

extension HomeViewController {
    
//    func getEmitterCells(){
//
//        var emitterCells = [CAEmitterCell]()
//
//        for index in 0..<10 {
//
//            let cell = CAEmitterCell()
//
//            cell.birthRate = 5
//            cell.lifetime = 10
//        }
//
//    }
}

// MARK: - Navigation methods
extension HomeViewController{
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationControllerOperation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        customNavigationController.duration = 1.0
        customNavigationController.reverse = operation == .pop
        return customNavigationController
    }
}
