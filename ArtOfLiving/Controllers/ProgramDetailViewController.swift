//
//  ProgramDetailViewController.swift
//  ArtOfLiving
//
//  Created by Dhayanithi on 20/02/18.
//  Copyright © 2018 Kuya. All rights reserved.
//

import UIKit

class ProgramDetailViewController: BaseViewController {

    @IBOutlet weak var backGroundImageView: UIImageView!
    @IBOutlet weak var imageContainerViewYConstraint: NSLayoutConstraint!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var descriptionContainerView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var separatorView: UIView!
    
    var backgroundImage = UIImage.init()
    var titleLabel : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        descriptionContainerView.backgroundColor = UIColor.clear
        containerView.backgroundColor = UIColor.clear
        profileImageView.layer.cornerRadius = 8.0
        profileImageView.clipsToBounds = true
        view.backgroundColor = UIColor.clear
        
        nameLabel.text = self.screenName
        nameLabel.alpha = 0
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //proceed
//        setTitleLabel()
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        
        animateVC()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
//        animateVC()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //proceed
//        animateVC()
        ClickedAnimatingFrame = self.profileImageView.frame
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
        
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setTitleLabel(){
        
        let titleLabelHeight : CGFloat = (navigationController != nil) ? navigationController!.navigationBar.frame.height : CGFloat(44.0)
        let titleLabelWidth : CGFloat = self.view.frame.size.width - 70
        let titleLabelY : CGFloat = (navigationController != nil) ? navigationController!.navigationBar.frame.minY : CGFloat(30.0)
        
        titleLabel = UILabel.init(frame: CGRect.init(x: clickedPoint.x, y: clickedPoint.y + (navigationController?.navigationBar.frame.maxY)!, width: titleLabelWidth, height: titleLabelHeight))
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.text = screenName
        titleLabel.backgroundColor = UIColor.clear
        titleLabel.textColor = UIColor.white
        titleLabel.font = UIFont.boldSystemFont(ofSize: getTitleTextFontSize())
        titleLabel.textAlignment = NSTextAlignment.center
        self.view.addSubview(titleLabel)
        
        UIView.animate(withDuration: 0.5, animations: {
            self.titleLabel.frame = CGRect.init(x: (self.view.frame.size.width - titleLabelWidth) / 2, y: 0, width: titleLabelWidth, height: titleLabelHeight)
        }) { (finished) in
            UIView.animate(withDuration: 0.5, animations: {
                self.titleLabel.frame = CGRect.init(x: (self.view.frame.size.width - titleLabelWidth) / 2, y: titleLabelY, width: titleLabelWidth, height: titleLabelHeight)
            }) { (finished) in
                //proceed
            }
            
            //            UIView.animate(withDuration: 5.25, delay: 0.0, usingSpringWithDamping: 0.2, initialSpringVelocity: 8.0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
            //                self.titleLabel.frame = CGRect.init(x: (self.view.frame.size.width - titleLabelWidth) / 2, y: titleLabelY, width: titleLabelWidth, height: titleLabelHeight)
            //
            //            }, completion: { (finished) in
            //                //proceed
            //            })
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ProgramDetailViewController {
    
    func animateVC(){
        
        let frame = imageContainerView.frame
        self.imageContainerView.frame = CGRect.init(x: 0, y: clickedFrame.origin.y, width: clickedFrame.size.width, height: clickedFrame.size.height)
        

        let separatorFrame = separatorView.frame
        separatorView.frame = CGRect.init(x: separatorFrame.origin.x, y: separatorFrame.origin.y, width: 0, height: separatorFrame.size.height)

        UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: UIViewAnimationOptions.curveEaseInOut, animations: {
            self.imageContainerView.frame = frame

            self.imageContainerView.layoutIfNeeded()
        }) { (finished) in
            if finished == true {
                //proceed
            }
        }
        
        UIView.animate(withDuration: 2.0) {
            self.nameLabel.alpha = 1.0
            self.separatorView.frame = separatorFrame
        }

        
//        UIView.animate(withDuration: 3.0, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: UIViewAnimationOptions.curveEaseInOut, animations: {
//            self.separatorView.alpha = 1.0
//
//            self.imageContainerView.layoutIfNeeded()
//        }) { (finished) in
//            if finished == true {
//                //proceed
//            }
//        }

    }
}

