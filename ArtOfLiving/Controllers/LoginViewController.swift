//
//  LoginController.swift
//  ArtOfLiving
//
//  Created by Dhayanithi on 10/02/18.
//  Copyright © 2018 Kuya. All rights reserved.
//

import UIKit
import AVFoundation

class LoginViewController: BaseViewController {

    var videoPlayer = AVPlayer()
    
    @IBOutlet weak var aolImageView: UIImageView!
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var userNameTextField: UITextField!
    
    @IBOutlet weak var backLoginButton: UIButton!
    @IBOutlet weak var loginHomeView: UIView!
    @IBOutlet weak var registerHomeButton: UIButton!
    @IBOutlet weak var loginHomeButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var loginViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var aolImageViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var loginHomeButtonWidthConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        videoViewSettings()
        loginViewSettings()
        loginHomeSettings()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        screenName = "Jai Gurudev"
        videoPlayer.play()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        addOrRemoveKeyboardNotifications(add: true)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        addOrRemoveKeyboardNotifications(add: false)
        animateBackToLoginViews()
        videoPlayer.pause()
        passwordTextField.text = ""
        userNameTextField.text = ""
    }
    
    func videoViewSettings(){
        
        let bundlePath = Bundle.main.path(forResource: "Art_of_Living", ofType: "mp4")
        let movieURL = URL(fileURLWithPath: bundlePath!)
        videoPlayer = AVPlayer.init(url: movieURL)
        let playerLayer = AVPlayerLayer(player: videoPlayer)
        playerLayer.frame = videoView.bounds
        playerLayer.videoGravity = AVLayerVideoGravity.resize
        
        videoView.layer.addSublayer(playerLayer)
        
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: videoPlayer.currentItem, queue: .main) { _ in
            self.videoPlayer.seek(to: CMTime.init(seconds: 0.90, preferredTimescale: CMTimeScale(CMTimeScale.bitWidth)))
            self.videoPlayer.play()
        }
        
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(hideKeyboard))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.delegate = self
        videoView.addGestureRecognizer(tapGesture)
    }
    
    func addOrRemoveKeyboardNotifications(add: Bool){
        
        if add{
            NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(playVideo), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        }
        else{
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        }
    }
    
    @objc func playVideo(notification: NSNotification) {
        videoPlayer.play()
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            loginViewBottomConstraint.constant = keyboardSize.height + 10
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        loginViewBottomConstraint.constant = 30
    }
    
    @objc func hideKeyboard() {
        
        passwordTextField.resignFirstResponder()
        userNameTextField.resignFirstResponder()
    }
    
    func loginViewSettings(){

        loginView.layer.cornerRadius = 10.0
        videoView.bringSubview(toFront: loginView)
        videoView.bringSubview(toFront: aolImageView)
        videoPlayer.volume = 0.0
        
        loginView.isHidden = true
        
        userNameTextField.backgroundColor = UIColor.clear
        passwordTextField.backgroundColor = UIColor.clear
        passwordTextField.isSecureTextEntry = true
        
        userNameTextField.font = UIFont.boldSystemFont(ofSize: 20)
        passwordTextField.font = UIFont.boldSystemFont(ofSize: 20)

        userNameTextField.attributedPlaceholder = NSAttributedString(string: "Username",
                                                               attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])

        passwordTextField.attributedPlaceholder = NSAttributedString(string: "Password",
                                                                     attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        
        loginButton.layer.cornerRadius = 5.0
        loginButton.layer.borderWidth = 0.65
        loginButton.layer.borderColor = UIColor.white.cgColor
        loginButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
    }
    
    func loginHomeSettings(){

        loginHomeView.isHidden = false

        loginHomeView.layer.cornerRadius = 10.0
        videoView.bringSubview(toFront: loginHomeView)
        
        loginHomeButton.layer.cornerRadius = 5.0
        loginHomeButton.layer.borderWidth = 0.65
        loginHomeButton.layer.borderColor = UIColor.white.cgColor
        loginHomeButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)

        registerHomeButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        registerHomeButton.layer.cornerRadius = loginHomeButton.layer.cornerRadius
        registerHomeButton.layer.borderWidth = loginHomeButton.layer.borderWidth
        registerHomeButton.layer.borderColor = UIColor.white.cgColor
        
        loginHomeButtonWidthConstraint.constant = (self.view.bounds.width - 60.0) / 2
        
        aolImageViewTopConstraint.constant = getAOLimageTopConstraint()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        loginHomeButtonWidthConstraint.constant = (self.view.bounds.width - 60.0) / 2
    }
    
    @IBAction func backLoginViewButtonClicked(_ sender: Any) {
        animateBackToLoginViews()
        hideKeyboard()
        passwordTextField.text = ""
        userNameTextField.text = ""
    }
    
    @IBAction func loginButtonClicked(_ sender: Any) {
        
        if passwordTextField.text?.isEmpty == false && userNameTextField.text?.isEmpty == false{
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController")
            let baseVC = viewController as? BaseViewController
            
            let navigationVC = UINavigationController.init(rootViewController: baseVC!)
            
            self.present(navigationVC, animated: true) {
                //proceed
            }
        }
        else{
            let alertController = UIAlertController.init(title: nil, message: "User name and Password cannot be empty!", preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func loginHomeButtonClicked(_ sender: Any) {

        loginButton.setTitle("Login",for: .normal)
        animateToLoginView()
    }
    
    @IBAction func registerButtonClicked(_ sender: Any) {
        
        loginButton.setTitle("Register",for: .normal)
        animateToLoginView()
    }
    
    func animateToLoginView(){
        
        var loginHomeFrame = loginHomeView.frame
        
        var loginViewFrame = loginView.frame
        loginViewFrame.origin.x = self.videoView.frame.width
        loginView.frame = loginViewFrame
        loginView.isHidden = false
        loginViewFrame.origin.x = 0
        
        loginHomeFrame.origin.x = loginHomeFrame.origin.x - self.videoView.frame.width
        self.backLoginButton.isHidden = true
        
        UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 8.0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
            self.loginHomeView.frame = loginHomeFrame
            self.loginView.frame = loginViewFrame
        }, completion: { (finished) in
            self.loginHomeView.isHidden = true
            self.backLoginButton.isHidden = false
        })

//        UIView.animate(withDuration: 1.0, animations: {
//            self.loginHomeView.frame = loginHomeFrame
//            self.loginView.frame = loginViewFrame
//        }) { (finished) in
//            self.loginHomeView.isHidden = true
//            self.backLoginButton.isHidden = false
//        }
    }
    
    func animateBackToLoginViews(){
        
        var loginHomeFrame = loginHomeView.frame
        
        var loginViewFrame = loginView.frame
        loginViewFrame.origin.x = self.videoView.frame.width
//        loginView.frame = loginViewFrame
//        loginView.isHidden = false
//        loginViewFrame.origin.x = loginHomeFrame.origin.x
        
//        loginHomeFrame.origin.x = loginHomeFrame.size.width
//        self.loginHomeView.frame = loginHomeFrame
        loginHomeFrame.origin.x = 0
        
        self.loginHomeView.isHidden = false
        
        UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 8.0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
            self.loginHomeView.frame = loginHomeFrame
            self.loginView.frame = loginViewFrame

        }, completion: { (finished) in
            self.loginView.isHidden = true
        })

        
//        UIView.animate(withDuration: 1.0, animations: {
//            self.loginHomeView.frame = loginHomeFrame
//            self.loginView.frame = loginViewFrame
//        }) { (finished) in
//            self.loginView.isHidden = true
//        }
    }
    
    func getAOLimageTopConstraint() -> CGFloat{
        
        if Utilities.getDeviceName() == DeviceNames.iPhone5{
            return 30.0
        }
        else if Utilities.getDeviceName() == DeviceNames.iPhone6To8{
            return 30.0
        }
        else if Utilities.getDeviceName() == DeviceNames.iPhone6STo8S{
            return 60.0
        }
        else if Utilities.getDeviceName() == DeviceNames.iPhoneX{
            return 60.0
        }
        
        return 30.0
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
