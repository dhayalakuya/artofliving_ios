//
//  Program+CoreDataClass.swift
//  ArtOfLiving
//
//  Created by Dhayanithi on 13/02/18.
//  Copyright © 2018 Kuya. All rights reserved.
//
//

import Foundation
import CoreData

public enum ProgramServerKey : String {
    case types              //Dictionary
    case id                 //Int
    case name               //String
    case description        //String
    case requirement        //String
    case is_active          //Boolean
    case lastupdated        //String
}

public enum ProgramKey : String {
    case serverId           //: Int
    case name               //: String
    case programDescription //: String
    case requirement        //: String
    case lastUpdated        //: String
    case isActive           //: Boolean
}


public class Program: NSManagedObject {

    class func programTypeWithId(programTypeId: Int, context: NSManagedObjectContext) -> Program {
        
        let idServer = NSNumber.init(value: programTypeId)
        
        let fetchRequest =  NSFetchRequest<NSFetchRequestResult>.init(entityName: "Program")
        fetchRequest.predicate = NSPredicate(format: "%@ == %K AND %@ == %K", idServer, ProgramKey.serverId.rawValue, NSNumber(value: true), ProgramKey.isActive.rawValue)
        
        let programs = try? context.fetch(fetchRequest)
        
        var program : Program
        
        if let aProgram = programs?.first as? Program {
            program = aProgram
        }
        else {
            program = NSEntityDescription.insertNewObject(forEntityName: String.init(describing: Program.self), into: context) as! Program
            program.serverId = Int64(programTypeId)
            
        }
        
        return program
    }
    
    class func getPrograms(context: NSManagedObjectContext) -> [Program] {
        
        let fetchRequest =  NSFetchRequest<NSFetchRequestResult>.init(entityName: "Program")
        fetchRequest.predicate = NSPredicate(format: "%@ == %K", NSNumber(value: true), ProgramKey.isActive.rawValue)
        
        let programs = try? context.fetch(fetchRequest)
        
        return programs as! [Program]
    }
}
