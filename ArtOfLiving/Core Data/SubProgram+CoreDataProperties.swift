//
//  SubProgram+CoreDataProperties.swift
//  ArtOfLiving
//
//  Created by Dhayanithi on 13/02/18.
//  Copyright © 2018 Kuya. All rights reserved.
//
//

import Foundation
import CoreData


extension SubProgram {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SubProgram> {
        return NSFetchRequest<SubProgram>(entityName: "SubProgram")
    }

    @NSManaged public var name: String?
    @NSManaged public var subProgramDescription: String?
    @NSManaged public var serverId: Int64
    @NSManaged public var typeId: Int64
    @NSManaged public var requirements: String?
    @NSManaged public var lastupdated: String?
    @NSManaged public var isActive: Bool
    @NSManaged public var program: Program?

}
