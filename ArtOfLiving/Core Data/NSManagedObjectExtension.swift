//
//  NSManagedObjectExtension.swift
//  ArtOfLiving
//
//  Created by Dhayanithi on 13/02/18.
//  Copyright © 2018 Kuya. All rights reserved.
//

import UIKit
import CoreData

class NSManagedObjectExtension: NSManagedObject {

    class func allObjectFromClass(aClass:AnyClass, context: NSManagedObjectContext) -> [AnyObject]? {
        
        let classString = String(describing: aClass.self)
        let fetchedRequest = NSFetchRequest<NSFetchRequestResult>.init(entityName: classString)
        fetchedRequest.predicate = NSPredicate(value:true)
        
        let objects = try? context.fetch(fetchedRequest)
        
        return objects! as [AnyObject]
    }
    
    class func deleteAllObjectFromClass(aClass:AnyClass, context: NSManagedObjectContext) {
        
        if var objects = self.allObjectFromClass(aClass: aClass, context: context) {
            
            print("Deleting \(objects.count) of class \(NSStringFromClass(aClass))")
            while objects.count > 0 {
                
                let managedObject = objects.removeLast() as! NSManagedObject
                context.delete(managedObject)
                
            }
            
            print("Deletion complete, all objects of class \(NSStringFromClass(aClass)) were deleted")
        }
        
    }
    
    func backgroundManagedObjectContext() -> NSManagedObjectContext {
        // Create background moc
        let backgroundContext = NSManagedObjectContext(concurrencyType: NSManagedObjectContextConcurrencyType.privateQueueConcurrencyType)
        // Set the document's moc as a parent to the background moc, so the changes are migrated properly
        backgroundContext.parent = self.managedObjectContext
        
        return backgroundContext
    }
}
