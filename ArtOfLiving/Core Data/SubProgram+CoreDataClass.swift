//
//  SubProgram+CoreDataClass.swift
//  ArtOfLiving
//
//  Created by Dhayanithi on 13/02/18.
//  Copyright © 2018 Kuya. All rights reserved.
//
//

import Foundation
import CoreData

public enum SubProgramServerKey : String {
    case subtypes              //Dictionary
    case id                 //Int
    case type_id            //Int
    case name               //String
    case requirements        //String
    case is_active          //Boolean
    case lastupdated        //String
}

public enum SubProgramKey : String {
    case serverId           //: Int
    case name               //: String
    case programDescription //: String
    case requirements        //: String
    case lastUpdated        //: String
    case isActive           //: Boolean
    case typeId            //Int
}


public class SubProgram: NSManagedObject {

    class func subProgramTypeWithId(subProgramTypeId: Int, context: NSManagedObjectContext) -> SubProgram {
        
        let idServer = NSNumber.init(value: subProgramTypeId)
        
        let fetchRequest =  NSFetchRequest<NSFetchRequestResult>.init(entityName: String.init(describing: SubProgram.self))
        fetchRequest.predicate = NSPredicate(format: "%@ == %K AND %@ == %K", idServer, SubProgramKey.serverId.rawValue, NSNumber(value: true), SubProgramKey.isActive.rawValue)
        
        let subPrograms = try? context.fetch(fetchRequest)
        
        var subProgram : SubProgram
        
        if let aSubProgram = subPrograms?.first as? SubProgram {
            subProgram = aSubProgram
        }
        else {
            subProgram = NSEntityDescription.insertNewObject(forEntityName: String.init(describing: SubProgram.self), into: context) as! SubProgram
            subProgram.serverId = Int64(subProgramTypeId)
            
        }
        
        return subProgram
    }
    
    class func subProgramTypeWithProgramId(programTypeId: Int, context: NSManagedObjectContext) -> [SubProgram] {
        
        let typeId = NSNumber.init(value: programTypeId)
        
        let fetchRequest =  NSFetchRequest<NSFetchRequestResult>.init(entityName: String.init(describing: SubProgram.self))
        fetchRequest.predicate = NSPredicate(format: "%@ == %K AND %@ == %K", typeId, SubProgramKey.typeId.rawValue, NSNumber(value: true), SubProgramKey.isActive.rawValue)
        
        let subPrograms = try? context.fetch(fetchRequest)
        return subPrograms as! [SubProgram]
    }
}
