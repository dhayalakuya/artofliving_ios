//
//  Program+CoreDataProperties.swift
//  ArtOfLiving
//
//  Created by Dhayanithi on 13/02/18.
//  Copyright © 2018 Kuya. All rights reserved.
//
//

import Foundation
import CoreData

extension Program {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Program> {
        return NSFetchRequest<Program>(entityName: "Program")
    }

    @NSManaged public var name: String?
    @NSManaged public var programDescription: String?
    @NSManaged public var serverId: Int64
    @NSManaged public var requirement: String?
    @NSManaged public var isActive: Bool
    @NSManaged public var lastUpdated: String?
    @NSManaged public var subProgram: SubProgram?

}
